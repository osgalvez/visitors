<div class="container" id="content-inside">
<div id="header" class='row'>
    <div class="col-md-12">
    <h1>Agregar Persona a Lista Negra</h1>
    </div>
</div>


<?php
    echo $this->Form->create($black_list_member);
    // Hard code the user for now.
    echo "<div class='row'>";
    echo "<div class='col-md-4'>";
    echo $this->Form->control('name', ['label' => 'Nombre(s)' , 'class' => 'form-control' , 'required' => true]);
    echo "</div>";
    echo "<div class='col-md-4'>";
    echo $this->Form->control('last_name', ['label' => 'Apellido Paterno' , 'class' => 'form-control', 'required' => true] );
    echo "</div>";
    echo "<div class='col-md-4'>";
    echo $this->Form->control('second_last_name', ['label' => 'Apellido Materno' , 'class' => 'form-control'] );    
    echo "</div>";
    echo "</div>";
    echo "<div class='row'>";
    echo "<div class='col-md-6'>";
    echo '<label for="country_id">País</label>';
    echo  $this->Form->select( 'country_id',$country_result, [
        'class' => 'form-control' ,'id' => 'select-country' ,'required' => true ,'default' => 148]);
    echo "</div>";
    echo "<div class='col-md-6'>";
    echo $this->Form->control('ssn', ['label' => 'Número de Identificación Único' , 'class' => 'form-control', 'required' => true] );
    echo "</div>";
    echo "<br>";
    echo "</div>";
    echo "<div class='row'>";
    echo "<div class=col-md-6>";
    echo $this->Form->control('reason', ['label' => 'Motivo' , 'class' => 'form-control', 'required' => true] );
    echo "</div>";
    echo "</div>";
    echo "<br>";
    echo "<div class='row'>";
    echo "<div class=col-md-12>";
    echo $this->Form->button('Guardar', ['type' => 'submit','id'=> 'btn-save' , 'class' => 'btn btn-success']);
    echo "</div>";
    echo "</div>";

    echo $this->Form->end();
?>
</div>





