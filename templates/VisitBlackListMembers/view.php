<!-- File: templates/Articles/view.php -->
<div class="section">
    <div class="row">
        <div class="col-md-12">
        <h1><?= h($member->name." ".$member->last_name." ".$member->second_last_name)  ?></h1>
        </div>
        <div class="col-md-12">
        <p>SSN: <?= h($member->ssn) ?></p>
        </div>
        <div class="col-md-12">
        <p>CURP: <?= h($member->curp) ?></p>
        </div>
        <div class="col-md-12">
        <p>Status: <?= h($member->status) ?></p>
        </div>
        <div class="col-md-12">
        <p><small>Created: <?= $member->insert_date->format(DATE_RFC850) ?></small></p>
        </div>
        <div class="col-md-12">

        <?= $this->Html->link('Regresar',  
    array('action' => 'index'), 
    array(
        'bootstrap-type' => 'primary',
        'class' => 'btn  btn-primary ',
        // transform link to a button
        'rule' => 'button'
    ) ) ;?>
        </div>
    </div>    <!-- File: templates/Articles/index.php -->
</div>



