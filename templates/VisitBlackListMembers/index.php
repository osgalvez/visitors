<!-- File: templates/Articles/index.php -->
<h1>Lista Negra</h1>
<?= $this->Html->link('Agregar', 
    array('action' => 'add'), 
    array(
        'bootstrap-type' => 'primary',
        'class' => 'btn  btn-primary ',
        // transform link to a button
        'rule' => 'button'
    )
); ?>


<table id="black-list-members" class="display">    
<thead> <tr>
        <th>Nombre(s)</th>
        <th>Apellido Paterno</th>
        <th>Apellido Materno</th>
        <th>Identificación</th>
        <th>Status</th>
        <th>Motivo</th>
        <th>Acción</th>



    </tr>
    </thead>
    <!-- Here is where we iterate through our $articles query object, printing out article info -->

    <?php foreach ($black_list_member as $member): ?>
    <tr>
       
        <td>
            <?= $this->Html->link($member->name, ['action' => 'view', $member->id]) ?>
        </td>
        <td>
        <?= $this->Html->link($member->last_name, ['action' => 'view', $member->id]) ?>
        </td>
        <td>
        <?= $this->Html->link($member->second_last_name, ['action' => 'view', $member->id]) ?>
        </td>
        <td>
        <?= $this->Html->link($member->ssn, ['action' => 'view', $member->id]) ?>
        </td>
        <td>
            <?= $member->status ?>
        </td>
        <td>
        <?= $this->Html->link($member->reason, ['action' => 'view', $member->id]) ?>
        </td>

        <td>
        <?php 
            if($member->status == 'Activo') {
                  echo $this->Html->link("<i class='fa fa-pause' aria-hidden='true'></i>", ['action' => 'disable', $member->id], 
                ['escape' => false] ) ;  
                
            } 
            else {
                   echo  $this->Html->link("<i class='fas fa-play'></i>", ['action' => 'enable', $member->id], 
                ['escape' => false] ) ;  

            }
            
             ?>
        </td>
      
   
    </tr>
    <?php endforeach; ?>
</table>
