<div class='container'>
    <div class='section'>
    <!-- Iterate over Employees In Plant -->
    <div class='card '>
            <div class="card-body text-center">
                <h2>Total Personas En Planta  <span class="badge badge-success"><?=  $total_employees+$count_visitor+$total_suppliers+$carriers_count+$contractors_c_count+$contractors_b_count+$contractors_a_count?></span></h2>
            </div>
        </div>

    <div class='card table-card'>
            <div class="card-body">
                <h2>Empleados en Planta  <span class="badge badge-primary"><?=  $total_employees?></span></h2>
                <table id="employees" class="display cell-border compact">    
                    <thead>
                        <tr>
                            <th>Area</th>
                            <th>Número</th>
                        </tr>
                    </thead>
                    <?php foreach ($employees_by_area as $key=>$employees): ?>
                    <tr>
                        <td>
                        <?= $employees['Area'] ?>
                        </td>
                        <td>
                        <?= $employees['Employees'] ?>
                        </td>
                    </tr>
                    <?php endforeach; ?>
                </table>
            </div>
        </div>
        <!-- Visitors in plant, add table and total count -->
        <div class='card table-card'>
            <div class="card-body">
                <h2>Visitantes  <span class="badge badge-primary"><?=  $count_visitor?></span></h2>
                <table id="contractor-employee" class="display cell-border compact">    
                    <thead>
                        <tr>
                            <th>Visitante</th>
                            <th>Areas de Visita</th>
                        </tr>
                    </thead>
                    <?php foreach ($visitors_decrypted as $key=>$visitor): ?>
                    <tr>
                        <td>
                        <?= $visitor['visitor'] ?>
                        </td>
                        <td>
                        <?= $visitor['VisitedAreas'] ?>
                        </td>
                    </tr>
                    <?php endforeach; ?>
                </table>
            </div>
        </div>
    <!-- Contractors Type A , count and detail table. -->
        <div class='card table-card'>
            <div class="card-body">
                <h2>Contratistas A <span class="badge badge-primary"><?=  $contractors_a_count?></span></h2>
                <table id="contractor-employee" class="display cell-border compact">    
                    <thead>
                        <tr>
                            <th>Contratista</th>
                            <th>Personas En Planta</th>
                        </tr>
                    </thead>
                    <?php foreach ($contractors_a as $key=>$contractor_a): ?>
                    <tr>
                        <td>
                        <?= $contractor_a['name'] ?>
                        </td>
                        <td>
                        <?= $contractor_a['number'] ?>
                        </td>
                    </tr>
                    <?php endforeach; ?>
                </table>
            </div>
        </div>
         <!-- Contractors B, count and Detail table group by Company -->
        <div class='card table-card'>
            <div class="card-body">
                <h2>Contratistas B <span class="badge badge-primary"><?=  $contractors_b_count?></span></h2>
                <table id="contractor-employee" class="display cell-border compact">    
                    <thead>
                        <tr>
                            <th>Contratista</th>
                            <th>Personas En Planta</th>
                        </tr>
                    </thead>
                    <?php foreach ($contractors_b as $key=>$contractor_b): ?>
                    <tr>
                        <td>
                        <?= $contractor_b['name'] ?>
                        </td>
                        <td>
                        <?= $contractor_b['number'] ?>
                        </td>
                    </tr>
                    <?php endforeach; ?>
                </table>
            </div>
        </div>
         <!-- Contractors C, count and Detail table group by Company -->
         <div class='card table-card'>
            <div class="card-body">
                <h2>Contratistas C <span class="badge badge-primary"><?=  $contractors_c_count?></span></h2>
                <table id="contractor-employee" class="display cell-border compact">    
                    <thead>
                        <tr>
                            <th>Contratista</th>
                            <th>Personas En Planta</th>
                        </tr>
                    </thead>
                    <?php foreach ($contractors_c as $key=>$contractor_c): ?>
                    <tr>
                        <td>
                        <?= $contractor_c['name'] ?>
                        </td>
                        <td>
                        <?= $contractor_c['number'] ?>
                        </td>
                    </tr>
                    <?php endforeach; ?>
                </table>
            </div>
        </div>
         <!-- Carriers, count and Detail table group by Company -->
         <div class='card table-card'>
            <div class="card-body">
                <h2>Transportistas <span class="badge badge-primary"><?=  $carriers_count?></span></h2>
                <table id="contractor-employee" class="display cell-border compact">    
                    <thead>
                        <tr>
                            <th>Transportistas</th>
                            <th>Personas En Planta</th>
                        </tr>
                    </thead>
                    <?php foreach ($carriers as $key=>$carrier): ?>
                    <tr>
                        <td>
                        <?= $carrier['name'] ?>
                        </td>
                        <td>
                        <?= $carrier['number'] ?>
                        </td>
                    </tr>
                    <?php endforeach; ?>
                </table>
            </div>
        </div>
          <!-- Visitors in plant, add table and total count -->
          <div class='card table-card'>
            <div class="card-body">
                <h2>Proveedores  <span class="badge badge-primary"><?=  $total_suppliers?></span></h2>
                <table id="contractor-employee" class="display cell-border compact">    
                    <thead>
                        <tr>
                            <th>Proveedor</th>
                            <th>Areas de Visita</th>
                        </tr>
                    </thead>
                    <?php foreach ($suppliers_decrypted as $key=>$visitor): ?>
                    <tr>
                        <td>
                        <?= $visitor['visitor'] ?>
                        </td>
                        <td>
                        <?= $visitor['VisitedAreas'] ?>
                        </td>
                    </tr>
                    <?php endforeach; ?>
                </table>
            </div>
        </div>

        
    </div>
</div>