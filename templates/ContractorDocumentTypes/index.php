<!-- File: templates/Articles/index.php -->
<h1>Tipos de Documentos</h1>
<?= $this->Html->link('Agregar', '/ContractorDocumentTypes/add' , [ 'class' => 'button', 'bootstrap-type' => 'primary',
        'class' => 'btn  btn-primary ',
        // transform link to a button
        'rule' => 'button'] ) ;?>
<table id="document-types" class="display">    
        <thead><tr>
        <th>Descripción</th>
        <th>Prefijo</th>
        <th>ID</th>
        <th>Status</th>
   

    </tr>
</thead>
    <!-- Here is where we iterate through our $articles query object, printing out article info -->

    <?php foreach ($documentTypes as $document): ?>
    <tr>
        <td>
            <?= $this->Html->link($document->description, ['action' => 'view', $document->id]) ?>
        </td>
        <td>
            <?= $this->Html->link($document->prefix, ['action' => 'view', $document->id]) ?>
        </td>
        <td>
            <?= $document->id ?>
        </td>
        <td>
            <?= $document->status ?>
        </td>
      
   
    </tr>
    <?php endforeach; ?>
</table>