<!-- File: templates/Articles/add.php -->

<h1>Agregar Tipo de Documento</h1>
<?php
    echo $this->Form->create($contractor_document_type);
    // Hard code the user for now.
    echo $this->Form->control('description', ['label' => 'Nombre' ,'class' => 'form-control']);
    echo $this->Form->control('prefix', ['label' => 'Prefijo' , 'class' => 'form-control'] );
    echo $this->Form->select(
        'type',
        ['Personal'=>'personal', 'Empresa'=>'empresa'],
        ['empty' => 'Tipo ...' ,'label' => 'Tipo','class' => 'form-control']);    
    echo $this->Form->button(__('Guardar') , [ 'class' => 'btn btn-success']);

    echo $this->Form->end();
?>