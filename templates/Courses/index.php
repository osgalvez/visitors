<!-- File: templates/Articles/index.php -->
<h1>Lista de Entrenamientos/Documentos de Información</h1>

<table id="visit" class="display">    
        <thead><tr>
        <th>Nombre Entrenamiento/Documento</th>
        <th>Año</th>
        <th>Tipo</th>
        <th>Ver</th>
    </tr>
    </thead>

    <!-- Here is where we iterate through our $articles query object, printing out article info -->

    <tr>
        <td>
        Inducción de Seguridad , Salud y Medio Ambiente
        </td>
        <td>
        2020
        </td>
        <td>
        Video
        </td>
        <td>
        <?= $this->Html->link("<i class='fas fa-video fa-2x'  title='Ver Video' ></i>", ['controller' => 'courses' , 'action' => 'view' , 1 ] , ['escape' => false] ) ?>
        
        </td>
    </tr>
    <tr>
        <td>
        Curso trabajar seguro al Conducir
        </td>
        <td>
        2020
        </td>
        <td>
        Video
        </td>
        <td>
        <?= $this->Html->link("<i class='fas fa-video fa-2x'  title='Ver Video' ></i>", ['controller' => 'courses' , 'action' => 'view' , 2 ] , ['escape' => false] ) ?>
        
        </td>
    </tr>
</table>