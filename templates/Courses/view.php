<!-- File: templates/Articles/view.php -->

<div class='container'>
<div class='section'>
<h3><?=$title?></h3>
<!-- File: templates/Articles/index.php -->
<?php
echo $this->Html->media($video_file, ['tag' => 'video', 'autoplay' => 'true' , 'controls' => 'true', 'id'=> 'video_container']);
?>
</div>
</div>