<!-- File: templates/Articles/add.php -->
<div class="container" id="content-inside">
<div id="header" class='row'>
    <div class="col-md-12">
    <h1>Visita Adicional</h1>
    </div>
</div>
<div id="" class='row'>
    <div class='col-md-12'>
        
    </div>
</div>
<?php
    echo $this->Form->create($visit , ['id' => 'additional_visit']);
    // Hard code the user for now.
    echo "<div class='row'>";
    echo "<div class='col-md-6'>";
    echo $this->Form->control('visited_person' , ["type" => 'text',"id" => 'visited_person' , 'label' => 'Persona a Quien Visita' , 'required'=>'true','class' => 'form-control']);
    echo "</div>";
    echo "<div class='col-md-6'>";
    echo $this->Form->control('reason', ['label' => 'Motivo' ,'id' => 'reason-id' ,'class' => 'form-control', 'required'=>'true']);
    echo "</div>";
    echo "</div>";

    echo "<br>";
   
    echo "<br>";
    echo "<div class='row'>";
    echo "<div class=col-6>";
    echo $this->Form->button('Enviar', ['type' => 'submit','id'=> 'btn-save' , 'class' => 'btn btn-success']);
    echo "</div>";

    echo "<div class=col-6>";

    echo $this->Html->link('Cancelar', ['action' => 'index' ] , [ 'rule' => 'button','class' => 'btn  btn-primary' ] ) ;
    echo "</div>";
    echo "</div>";

    echo $this->Form->end();
?>
</div>