<!-- File: templates/Articles/index.php -->

<div id="header" class='row'>
    <div class="col-md-12">
         <h1>Mis Visitas</h1>
    </div>
</div>
<div id="" class='row'>
    
</div>
<br>
<div class='table-responsive'>
<table id="visit" class="display">    
<thead><tr>
        <th>Visitante</th>
        <th>Motivo</th>
        <th>A quién visita</th>
        <th>Área de Visita</th>
        <th>Fecha de Entrada</th>
        <th>Salida aprobada</th>
        <th>Fecha de Salida</th>
        <th>Aprobar Salida</th>

        </tr>
</thead>


    <!-- Here is where we iterate through our $articles query object, printing out article info -->

    <?php  foreach ($my_visits as $visit):  ?>
    <tr>
        <td>
            <?= $visit->visit_visitors['first_name']." ".$visit->visit_visitors['last_name']." ".$visit->visit_visitors['second_last_name'] ; ?>
        </td>
        <td>
            <?= $visit->reason; ?>
        </td>
        <td>
            <?= $visit->visited_person?>
        </td>
        <td>
            <?= $visit->status ?>
        </td>
        <td>
            <?= $visit->entry_date->nice() ?>
        </td>
        <td>
            <?= ($visit->approved_exit == 1  ) ? 'Si' : 'No'  ?>
        </td>
        <td>
            <?= is_null($visit->exit_date) ? '' : $visit->exit_date->nice()  ?>
        </td>
        <td>
        <?php if($visit->status == 'active') 
                {
                    if($visit->approved_exit == 0 ) {
                        echo $this->Html->link('Aprobar Salida', ['action' => 'approveExit', $visit->id]  , [ 'rule' => 'button','class' => 'btn  btn-success' ] ) ;
                    }
                
                    }  
                    else { echo (""); }  ?>

        </td>
        
    </tr>
    <?php endforeach; ?>
</table>

</div>

