<!-- File: templates/Articles/add.php -->
<div class="container" id="content-inside">
<div id="header" class='row'>
    <div class="col-md-12">
    <h1>Agregar Visita</h1>
    </div>
</div>
<div id="" class='row'>
    <div class='col-md-12'>
        
    </div>
</div>
<?php
    echo $this->Form->create($visit , ['id' => 'signatureform', 'type' => 'file']);
    // Hard code the user for now.
    echo "<div class='row'>";
        echo "<div class='col-12'>";
    echo  $this->Form->select('visit_visitor_id', $visitor_result, [
        'empty' => 'Visitante' ,'default' => $visitorid , 'class' => 'form-control' ,'id' => 'select-visitor' , 'label' => 'Visitante']  );
        echo "</div>";
    echo "<div class='col-md-6'>";
    echo $this->Form->control('visited_person' , ["type" => 'text',"id" => 'visited_person' , 'label' => 'Persona a Quien Visita' , 'required'=>'true','class' => 'form-control']);
    echo "</div>";
    echo "<div class='col-md-6'>";
    echo $this->Form->control('reason', ['label' => 'Motivo' ,'id' => 'reason-id' ,'class' => 'form-control', 'required'=>'true']);
    echo "</div>";
    echo "</div>";

    /*  echo '<label for="nationality">Nationality</label>';
    echo $this->Form->select(
        'nationality',
        ['Mexican'=>'Mexican', 'Japanese'=>'Japanese', 'Hungarian'=>'Hungarian', 'North American'=>'North American', 'Other'=>'Other'],
        ['empty' => 'Nationality ...']
    );*/
    echo "<br>";
    echo "<div class='row'>";
    echo "<div class=col-sm-2>";
    echo "<img id='logo-zoltek' src='/VisitorsApp/img/zolt_logo.png'/>";
    echo "</div>";
    echo "<div id='terms' class=col-sm-10>";
    echo '<p>El  suscrito, me  comprometo  a mantener  bajo  estricta  confidencialidad toda información   técnica,  comercial,  legal  y  de negocios recibida y/o conocida durante esta y en  las futuras visitas que realice a las instalaciones de Zoltek de México y sus afiliadas. 
    Además me obligo a no enajenar, arrendar, prestar, gravar, negociar, revelar, publicar, grabar, mostrar, dar a conocer, tomar videos, audios o fotografías, transmitir o de alguna otra forma divulgar o proporcionar por cualquier medio, la Información Confidencial   a  ningún  tercero,  sin  el consentimiento escrito de Zoltek de México y/o sus afiliadas del grupo Toray.
    Solo usaré la información Confidencial proporcionada por Zoltek y/o sus afiliadas del grupo Toray con la finalidad para la cual me sea revelada.
    Acepto que la vigencia de las obligaciones de Confidencialidad iniciará a partir de la fecha en que reciba por primera vez la Información Confidencial, y surtirá sus efectos durante todo el tiempo en que conserven tal carácter.
    El suscrito me obligo a seguir  las políticas internas y reglas de seguridad durante nuestra estancia en las instalaciones de  Zoltek  de  México  y/o  sus  afiliadas  del  Grupo Toray.
    Acepto que en caso de inobservancia de las políticas internas y reglas de seguridad, será  mi responsabilidad  indemnizar a   Zoltek de México y/o sus afiliadas, por los daños causados.
    Manifiesto bajo protesta de decir verdad que la información  y documentos de identificación aquí señalados son verdaderos y vigentes y que fue puesto a mí disposición el Aviso de privacidad de la empresa y que puede ser consultado a través del portal www.zoltek.mx, en el que se hace alusión al tratamiento que se le darán a los datos  del suscrito(a). Autorizo el tratamiento de mis datos personales incluyendo los sensibles,  de conformidad con el artículo 9 de la Ley Federal de Protección de Datos personales en posesión de los particulares y artículo 15 de su Reglamento, y que conozco el procedimiento para hacer valer mis derechos de Acceso, Rectificación, Cancelación y Oposición de mi Datos personales.
    </p>';
    echo "</div>";
    echo "</div>";
    echo "<br>";
    echo "<div class='row'>";
    echo "<div class=col-1>";
    echo $this->Form->checkbox('terms_accepted' , ['value' => 'Y','hiddenField' => 'N', 'id' => 'terms','required'=>'true',] );
    echo "</div>";
    echo "<div class=col-11>";
    echo '<label for="terms_accepted">He Leido y Acepto los terminos y condiciones</label>';
    echo "</div>";
    echo "</div>";
    echo $this->Form->hidden('signature', ["id" => 'signature']);
    echo "<div class='row'>";
    echo "<div class=col-12>";
    echo '<div id="canvasDiv"></div>';
    echo "</div>";
    echo "</div>";
    echo "<div class='row'>";
    echo "<div class=col-6>";
    echo $this->Form->button('Enviar', ['type' => 'button','id'=> 'btn-save' , 'class' => 'btn btn-success']);
    echo "</div>";

    echo "<div class=col-6>";

    echo $this->Form->button('Borrar Firma', ['type' => 'button' ,'id' => 'reset-btn' , 'class' => 'btn btn-warning']);
    echo "</div>";
    echo "</div>";

    echo $this->Form->end();
?>
</div>