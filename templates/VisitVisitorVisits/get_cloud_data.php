<!-- File: templates/Articles/index.php -->
<div id="header" class='row'>
    <div class="col-md-12">
         <h1>Visitantes en Planta</h1>
    </div>
</div>
<div id="" class='row'>
<div class="col-md-12">

<h3 class="p-3 mb-2 bg-success text-white" >Total de Visitantes en Planta : <?= $total_visits ; ?> </h3>        
</div>
</div>
<br>
<div class='table-responsive'>
<table id="visit-cloud" class="display">    
<thead><tr>
        <th>Visitante - Empresa</th>
        <th>A quién visita</th>
        <th>Áreas de Visita</th>
        <th>Fecha de Entrada</th>

        </tr>
</thead>


    <!-- Here is where we iterate through our $articles query object, printing out article info -->

    <?php  foreach ($decripted_data as $key => $visit):  ?>
    <tr>
        <td>
            <?= $visit['visitor'] ; ?>
        </td>
        <td>
        <?= $visit['person'] ; ?>
        </td>
        <td>
        <?= $visit['areas'] ; ?>
        </td>
        <td>
        <?= $visit['entry_date'] ; ?>
        </td>
   
    </tr>
    <?php endforeach; ?>
</table>

</div>

