<!-- File: templates/Articles/index.php -->

<div id="header" class='row'>
    <div class="col-md-12">
         <h1>Visitas</h1>
    </div>
</div>
<div id="" class='row'>
    <div class='col-md-12'>
        <?= $this->Html->link('Agregar Visita', 
        array('action' => 'add'), 
        array(
        'bootstrap-type' => 'primary',
        'class' => 'btn  btn-primary ',
        // transform link to a button
        'rule' => 'button'
        )
        ); ?>
    </div>
</div>
<br>
<div class='table-responsive'>
<table id="visit" class="display">    
<thead><tr>
        <th>Visitante</th>
        <th>Motivo</th>
        <th>A quien visita</th>
        <th>Area de Visita</th>
        <th>Fecha de Entrada</th>
        <th>Salida aprobada</th>
        <th>Fecha de Salida</th>
        <th>Salida</th>

        </tr>
</thead>


    <!-- Here is where we iterate through our $articles query object, printing out article info -->
        
    <?php   foreach ($visits as $visit):  ?>
    <tr>
        <td>
            <?= $visit->visit_visitors['first_name']." ".$visit->visit_visitors['last_name']." ".$visit->visit_visitors['second_last_name'] ; ?>
        </td>
        <td>
            <?= $visit->reason; ?>
        </td>
        <td>
            <?= $visit->visited_person?>
        </td>
        <td>
            <?= $visit->area ?>
        </td>
        <td>
            <?= $visit->entry_date->nice() ?>
        </td>
        <td>
            <?= ($visit->approved_exit == 1  ) ? 'Si' : 'No'  ?>
        </td>
        <td>
        <?= is_null($visit->exit_date) ? '' : $visit->exit_date->nice()  ?>
        </td>
        <td>
        <?php if($visit->status == 'active') 
                {
                    if($visit->approved_exit == 0 ) {
                        echo $this->Html->link('Registrar Salida', ['action' => 'visitExit', $visit->id]  , [ 'rule' => 'button','class' => 'btn  btn-warning', 'onclick' => 'return false' , 'disabled' => 'disabled'] ) ;
                    }
                    else {
                        echo $this->Html->link('Registrar Salida', ['action' => 'visitExit', $visit->id]  , [ 'rule' => 'button', 'class' => 'btn  btn-success'] ) ;

                    }
                    }  
                    else { echo (""); }  ?>

        </td>
        
    </tr>
    <?php endforeach; ?>
</table>

</div>

