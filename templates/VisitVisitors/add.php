<!-- File: templates/Articles/add.php -->

<h1>Registrar Visitante</h1>
<?php
// Hard code the user for now.
    echo $this->Form->create($visitor, ['type' => 'file'] );
    // Hard code the user for now.
    echo $this->Form->control('first_name' , ['label' => 'Nombre' , 'class' => 'form-control' , 'required' => true ]);
    echo $this->Form->control('last_name' , ['label' => 'Apellido Paterno' , 'class' => 'form-control' , 'required' => true  ]);
    echo $this->Form->control('second_last_name' , ['label' => 'Apellido Materno' , 'class' => 'form-control' ]);

    echo '<label for="nationality">País</label>';
    echo  $this->Form->select( 'nationality',$country_result, [
        'class' => 'form-control' ,'id' => 'select-country' ,'required' => true ,'default' => 148]);
        echo $this->Form->control('other_country' , ['id'=> 'other_country','label' => 'Otro País' , 'class' => 'form-control col-6' , 'disabled' => 'true' ]);
        echo '<label for="support_email">Email de Respando</label>';
        echo $this->Form->file('support_email', ['id'=>'support_email','disabled' => 'true' , 'class'=> 'col-6 form-control']);
        echo $this->Form->control('phone' , ['label' => 'Teléfono' , 'class' => 'form-control' ]);
        echo $this->Form->control('email' , ['label' => 'E-Mail' , 'class' => 'form-control' ]);
        echo $this->Form->control('company' , ['label' => 'Empresa' , 'class' => 'form-control' ]);
        echo $this->Form->button('Guardar', ['type' => 'submit','id'=> 'btn-save' , 'class' => 'btn btn-success']);
        echo $this->Form->end();
?>