<!-- File: templates/Articles/index.php -->
<h1>Lista de Visitantes Registrados </h1>

<?= $this->Html->link('Registrar Visitante', 
array('action' => 'add'), 
array(
    'bootstrap-type' => 'primary',
    'class' => 'btn  btn-primary ',
    // transform link to a button
    'rule' => 'button'
)
); ?>
<table id="visito" class="display">    
        <thead><tr>
        <th>Nombre(s)</th>
        <th>Apellido Materno</th>
        <th>Apellido Paterno</th>
        <th>País</th>
        <th>Teléfono</th>
        <th>E-Mail</th>
        <th>Empresa</th>
        <th>Acciones</th>

    </tr>
</thead>
    <!-- Here is where we iterate through our $articles query object, printing out article info -->

    <?php      foreach ($visitors as $visitor): ?>
    <tr>
        <td>
            <?= $this->Html->link($visitor->first_name, ['action' => 'view', $visitor->id]) ?>
        </td>
        <td>
            <?= $this->Html->link($visitor->last_name, ['action' => 'view', $visitor->id]) ?>
        </td>
        <td>
            <?= $this->Html->link($visitor->second_last_name, ['action' => 'view', $visitor->id]) ?>
        </td>
        <td>
            <?= $visitor->visit_allowed_countries['country_name'] ?>
        </td>
        <td>
            <?= $visitor->phone ?>
        </td>
        <td>
            <?= $visitor->email ?>
        </td>
        <td>
            <?= $visitor->company ?>
        </td>
        <td>
        <?php 
            $session = $this->getRequest()->getSession(); 
             if( $session->read('Group.SG_Apps_Visitors_User') == 1 ) {echo $this->Html->link("<i class='fa fa-calendar'  title='Registrar Visita'></i>", ['controller' => 'visit_visitor_visits','action' => 'add', $visitor->id], ['escape' => false] );} ?>  
        <?= $this->Html->link("<i class='fas fa-address-card'  title='Agregar Identificacion' ></i>", ['controller'=>'visit_id_documents' , 'action' => 'add', $visitor->id], ['escape' => false] ) ?>  
        </td>
        
    </tr>
    <?php endforeach; ?>
</table>