<!-- File: templates/Articles/view.php -->

<h1><?= h($visitor->first_name." ".$visitor->last_name." ".$visitor->second_last_name)  ?></h1>
<p>Telefono: <?= h($visitor->phone) ?></p>
<p>Email: <?= h($visitor->email) ?></p>
<p>Compañia: <?= h($visitor->company) ?></p>
<p><small>Created: <?= $visitor->created->format(DATE_RFC850) ?></small></p>

<?php 
if(count($result) > 0 ) {
    echo "<div><h3>Identificaciones </h3>";
    foreach($result as $key=> $value ){
        $html = '<img class="id_image" src="data:'.$value['file_type'].';base64, '.$value['image'].'" />';
        echo $html;
    }

}

if($visitor['support_file_type'] != '' ) {
    echo '<a download="support.msg" href="data:'.$visitor['support_file_type'].';base64, '.$visitor['support_email'].'" >Descargar Archivo de Soporte</a>';
}



echo '</div>';
echo $this->Html->link('Agregar Visita' , '/VisitVisitorVisits/add' , ['class' => 'button','bootstrap-type' => 'primary',
'class' => 'btn  btn-primary ',
// transform link to a button
'rule' => 'button']);
?>
