<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 * @var \App\View\AppView $this
 */

$cakeDescription = 'Zoltek de Mexico - Visitors App';
?>
<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>
        <?= $cakeDescription ?>:
        <?= $this->fetch('title') ?>
    </title>
    <?= $this->Html->meta('icon') ?>


    <?= $this->Html->script('jquery-1.12.4.js'); ?>
    <?= $this->Html->script('jquery-ui.js'); ?>
    <?= $this->Html->script('jquery.dataTables.min.js') ?>
    <?= $this->Html->script('popper.min.js') ?>
    <?= $this->Html->script('bootstrap.min.js') ?>

    

    <?= $this->Html->script('visitor.js'); ?>
    <?= $this->Html->script('visitor_visits.js'); ?>
    <?= $this->Html->script('signature.js'); ?>
    <?= $this->Html->script('html2canvas.min.js'); ?>
    <?= $this->Html->css('cake.css') ?>
   
    <?= $this->Html->css('bootstrap.min.css') ?>
    <?= $this->Html->css('jquery.dataTables.css') ?>


    <?= $this->Html->css('all.css') ?>
    <?= $this->Html->css('custom.css') ?>
    <?= $this->Html->css('jquery-ui.css') ?>
    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>
</head>
<body>



<nav class="navbar navbar-expand-lg navbar-light bg-light">
<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
        <div class="top-nav-title">
            <a  class="" href=""><span>Zoltek</span>Visitor</a>
        </div>
   
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">

        
        
        
    <?php 
             $session = $this->getRequest()->getSession();
             $name = $session->read('UserInfo.CompanyID');
        
             if($name != 'admin' && $name != '') {
                
                /*Administrativo*/
            
            echo '<li class="nav-item dropdown">';
            echo '<a class="nav-link dropdown-toggle" href="#" id="navbarDropdownContractors" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">';
            echo 'Mi Información  </a>';
            echo '<div class="dropdown-menu" aria-labelledby="navbarDropdownContractors">';
            echo $this->Html->link("Empresa" , ['controller'=>'visit_companies' , 'action' => 'index'], ["class"=>"dropdown-item" ]) ;
            echo '</div></li>';
            echo "<li class='nav-item'>";
            echo $this->Html->link("Entrenamientos" , ['controller'=>'courses' , 'action' => 'index'] , ['class' => 'nav-link']);
            echo "</li>";
            echo "<li class='nav-item'>";
            echo $this->Html->link("Cerrar Sesion" , ['controller'=>'users' , 'action' => 'logout'] , ['class' => 'nav-link', 'bootstrap-type' => 'secondary',
            'class' => 'btn  btn-secondary ',
            // transform link to a button
            'rule' => 'button']);
            echo "</li>";
            echo "<li class='nav-item'>";
            echo  "<span class='nav-link'>Usuario : ". $name."</span>";
            echo "</li>";

             }

             else {
                if($name == 'admin') {
                    echo "<li class='nav-item'>";
                echo $this->Html->link("Personal En Planta" , ['controller'=>'head-count' , 'action' => 'index'] , ['class' => 'nav-link']);
                echo "</li>";
                echo "<li class='nav-item'>";
            echo $this->Html->link("Cerrar Sesion" , ['controller'=>'users' , 'action' => 'logout'] , ['class' => 'nav-link', 'bootstrap-type' => 'secondary',
            'class' => 'btn  btn-secondary ',
            // transform link to a button
            'rule' => 'button']);
            echo "</li>";
            echo "<li class='nav-item'>";
            echo  "Usuario : ". $name;
            echo "</li>";
                }
             } 
        
             ?>        
        
        </ul>
  </div>
</nav>
    <main class="main">
        <div class="container-fluid">
            <?= $this->Flash->render() ?>
            <?= $this->fetch('content') ?>
        </div>
    </main>
    <footer>
    </footer>
</body>
</html>
