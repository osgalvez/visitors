<!-- File: templates/Articles/index.php -->
<h1>Mis Visitas Programadas</h1>
<?= $this->Html->link('Programar', '/VisitScheduledVisits/add' , [ 'class' => 'button'] ) ;?>
<table id="visit" class="display">    
        <tr>
        <th>Visitante</th>
        <th>Razon</th>
        <th>Area</th>
        <th>Fecha </th>
        <th>Hora</th>

        <th>Status</th>


    </tr>

    <!-- Here is where we iterate through our $articles query object, printing out article info -->

    <?php  foreach ($scheduled_visits as $visit): ?>
    <tr>
        <td>
            <?= $visit->visit_visitor_id ?>
        </td>
        <td>
            <?= $visit->reason; ?>
        </td>
        <td>
            <?= $visit->area?>
        </td>
        <td>
            <?= $visit->schedule_date ?>
        </td>

        <td>
            <?= $visit->schedule_time  ?>
        </td>
        <td>
            <?= $visit->status ?>
        </td>
        <td>
            <?= $visit->exit_date ?>
        </td>
        <td>
        <?php if($visit->status == 'active') 
                {
                    if($visit->approved_exit == 0 ) {
                        echo $this->Html->link('Registrar Salida', ['action' => 'visitExit', $visit->id]  , [ 'class' => 'button', 'onclick' => 'return false' , 'disabled' => 'disabled'] ) ;
                    }
                    else {
                        echo $this->Html->link('Registrar Salida', ['action' => 'visitExit', $visit->id]  , [ 'class' => 'button'] ) ;

                    }
                    }  
                    else { echo (""); }  ?>

        </td>
        
    </tr>
    <?php endforeach; ?>
</table>