<!-- File: templates/Articles/add.php -->

<h1>Agregar Visita Programada</h1>
<?php
    echo $this->Form->create($scheduled_visit);
    // Hard code the user for now.
    echo '<label for="visit_visitor_id">Visitor </label>';
    echo  $this->Form->select('visit_visitor_id', $visitor_result, [
        'empty' => 'Visitor' ]);
    echo $this->Form->control('schedule_date', ['label' => 'Fecha ','type' => 'date']);
    echo $this->Form->control('schedule_time', ['label' => 'Hora','type' => 'time']);
    echo $this->Form->control('reason' , ['label' => 'Motivo de Visita']);
    
    echo $this->Form->button(__('Guardar Visita'));
    echo $this->Form->end();
?>