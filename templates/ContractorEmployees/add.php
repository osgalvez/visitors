<!-- File: templates/Articles/add.php -->
<!-- File: templates/Articles/add.php -->
<div class='container'>
  <div class='section-inside'>
<h1>Agregar Personal</h1>
<span class="required-field">(*) Campos requeridos</span>
<?php
    echo $this->Form->create($contractor_employee);
    // Hard code the user for now.
    echo $this->Form->control('visit_company_id', ['type' => 'hidden', 'value' => $company_id]);
    echo $this->Form->control('name', ['label' => 'Nombre(s) (*) ', 'required'=> true ,'class' => 'form-control' , 'type' => 'text'] );
    echo $this->Form->control('last_name', ['label' => 'Apellido Paterno (*)' , 'required'=> true , 'class' => 'form-control', 'type' => 'text']);
    echo $this->Form->control('second_last_name' , ['label' => 'Apellido Materno' ,'class' => 'form-control', 'type' => 'text']);
    echo $this->Form->control('ssn', ['label' => 'Numero de Seguridad Social (*)' , 'required'=> true , 'class' => 'form-control',  'type' => 'number']);
    echo '<br>';
    echo $this->Form->button(__('Guardar') , [ 'class' => 'btn btn-success']);
    echo $this->Html->link('Cancelar', '/VisitCompanies/index' , [ 'class' => 'button', 'bootstrap-type' => 'danger',
        'class' => 'btn  btn-danger ',
        'rule' => 'button'] ) ;
    echo $this->Form->end();
?>

</div>
</div>