<!-- File: templates/Articles/view.php -->

<div class='container'>
<div class='section'>
<h1><?= h($employee['name']." ".$employee['last_name']." ".$employee['second_last_name'])  ?></h1>
<p>Status: <?= h($employee['status']) ?></p>
<!-- File: templates/Articles/index.php -->

        

        </tr>
</thead>
<?php
if(count($result) > 0 ) {
    echo "<div>";
   echo  " <span>Significado de status</span>
   <div class='row'>
       <div class='col-md-1'>
           <i class='warning fas fa-exclamation-triangle' ></i> 
       </div>
       <div class='col-md-3'>
           Documento Registrado sin Validar
       </div>
           <div class='col-md-1'>
               <i class='good fas fa-check-circle' alt=''></i> 
           </div>
           <div class='col-md-3'>
               Documento Valido
           </div>
           <div class='col-md-1'>
               <i class='bad fas fa-times-circle' alt=''></i> 
           </div>
           <div class='col-md-3'>
               Documento No Válido
           </div>
   </div>   
   <hr/>
   <h3>Expediente </h3>";

   echo  $this->Html->link("<i class='fas  add-side fa-2x fa-plus-circle' title='Agregar Documento'></i>", ['controller' => 'contractor_employee_documents','action' => 'add',$employee['id']], ['escape' => false] );
   echo "<div class='table-responsive'>
        <table id='visit' class='display'>    
        <thead>
            <tr>
            <th>Tipo Documento</th>
            <th>Tipo Archivo</th>
            <th>Status</th>
            <th>Archivo</th>
        </thead>";
    foreach($result as $key=> $value ){
        $icono_status = "";
        $icono_editable="";
        if($value['status'] === 'Registrado') {
            $icono_status = "<i class='warning fas fa-exclamation-triangle' title ='Documento en Proceso de Validacion' ></i> ";
        }
        if($value['status'] === 'Valido') {
            $icono_status = "<i class='good fas fa-check-circle' title ='Documento Válido'></i>  ";

        }
        if($value['status'] === 'No Valido') {
            $icono_status = " <i class='bad fas fa-times-circle' title ='Documento No Válido'></i> ";

        }
        if($value['editable'] == 1 ) {
            
            $icono_editable = $this->Html->link('<i class="fas fa-2x fa-edit" title = "Editar Documento"></i>', ['controller'=>'contractor_employee_documents' , 'action' => 'edit', $value['id'] ], ['escape' => false] ) ;

        } 
        echo "<tr>";
        echo "<td>".$value['contractor_document_types']['description']."</td>";
        echo "<td>".$value['extension']."</td>";
        echo "<td style='text-align: center;'>".$icono_status."</td>";
        echo "<td>";
        echo  $this->Html->link('<i class="fas fa-2x fa-eye" title = "Ver Documento" ></i>', ['controller'=>'contractor_employee_documents' , 'action' => 'view', $value['id'] ], ['escape' => false] ) ;
        echo $icono_editable;
        echo "</td>";
        echo '</tr>';
    }
    echo '</table>';

}

echo $this->Html->link('Regresar', '/VisitCompanies/view/'.$employee['visit_company_id'] , [ 'class' => 'button', 'bootstrap-type' => 'primary',
        'class' => 'btn  btn-primary ',
        'rule' => 'button'] ) ;

?>
</div>
</div>