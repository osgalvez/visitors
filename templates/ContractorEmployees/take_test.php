
<div class='container'>

<div class="section">
<h1>Seleccionar Entrenamiento</h1>

<?php
    echo $this->Form->create();
    echo $this->Form->select(
        'training_id',
        $available_training,
        ['empty' => '(Elegir Entrenamiento)' , 'required' => true , 'class' => 'form-control']
    );
    echo"<br>";
    echo $this->Form->control('contractor_employee_id', ['type' => 'hidden', 'value' => $employee_id]);
    echo $this->Form->button(__('Enviar') , [ 'class' => 'btn btn-success' , 'type'=> 'submit']);
    echo $this->Form->end();
?>
</div>
</div>