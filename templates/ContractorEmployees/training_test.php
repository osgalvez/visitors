
<div class='container'>

<div class="section-inside">
<div class="card" > 
<div class="card-body">
<h3><?=$training_name?></h3>
<b>Nombre : </b><span><?=$employee_name?></span>
</div>
</div>
<?php
    echo $this->Form->create();
    echo $this->Form->control('training_id', ['type' => 'hidden', 'value' => $training_id]);
    echo $this->Form->control('employee_id', ['type' => 'hidden', 'value' => $employee_id]);

    // Hard code the user for now.
    foreach($questions as $question) {
        echo '<div class="card" ">';
        echo '<div class="card-body">';
        echo "<span>".$question['text']."</span>";
        echo '<div class="form-check">';
        echo $this->Form->radio(
            $question['id'],
            [
                ['value' => $answers[$question['id']][0]['id'], 'text' => $answers[$question['id']][0]['text']]
            ] ,  ['required' => true , 'hiddenField' => false]
        );
        echo "</div>";
        echo '<div class="form-check">';
        echo $this->Form->radio(
            $question['id'],
            [
                ['value' => $answers[$question['id']][1]['id'], 'text' => $answers[$question['id']][1]['text']]
            ] ,  ['required' => true , 'hiddenField' => false]
        );
        echo "</div>";
        echo '<div class="form-check">';
        echo $this->Form->radio(
            $question['id'],
            [
         
                ['value' => $answers[$question['id']][2]['id'], 'text' => $answers[$question['id']][2]['text']]
                ] ,  ['required' => true , 'hiddenField' => false]
            
        );
        echo "</div>";
        echo "</div>";
        echo "</div>";

    } 
    echo $this->Form->button(__('Guardar') , [ 'class' => 'btn btn-success']);
    echo $this->Form->end();
?>


</div>
</div>