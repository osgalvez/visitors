<!-- File: templates/Articles/index.php -->
<h1>Plantilla Contratistas</h1>
<?= $this->Html->link('Agregar', '/ContractorEmployees/add' , [ 'class' => 'button'] ) ;?>
<table id="contractor-employee" class="display">    
        <tr>
        <th>Nombre</th>
        <th>Apellido Paterno</th>
        <th>Apellido Materno</th>
        <th>Numero IMSS</th>
        <th>Status</th>
   

    </tr>

    <!-- Here is where we iterate through our $articles query object, printing out article info -->

    <?php foreach ($visitors as $visitor): ?>
    <tr>
        <td>
            <?= $this->Html->link($visitor->name, ['action' => 'view', $visitor->id]) ?>
        </td>
        <td>
            <?= $this->Html->link($visitor->last_name, ['action' => 'view', $visitor->id]) ?>
        </td>
        <td>
            <?= $this->Html->link($visitor->second_last_name, ['action' => 'view', $visitor->id]) ?>
        </td>
        <td>
            <?= $visitor->ssn ?>
        </td>
        <td>
            <?= $visitor->status ?>
        </td>
  
   
    </tr>
    <?php endforeach; ?>
</table>