
<div class='container'>

<div class="section-inside">
<div class="card" > 
<div class="card-body">
<span><b> Correctas : </b><?= $correct  ?> </span>
<span><b> Inrrectas : </b><?= $incorrect_count  ?> </span>
<span><b> Puntaje : </b><?= ($correct/10) * 100  ?> </span>

</div>
</div>

<?php

    // Hard code the user for now.
    foreach($results as $question_number => $question) {
        echo '<div class="card" ">';
        echo '<div class="card-body">';
        if($question['correct']) {
            echo "<div><span class='good'><i class='fas fa-2x fa-check good'></i></span></div>";
        } 
        else {
            echo "<div><span class='bad'><i class='fas fa-2x fa-times bad'></i></span></div>";

        }
        echo "<span><b>Pregunta : </b>".$question['question']."</span>";
        echo "<br>";
        echo "<span><b>Respuesta : </b>".$question['answer']."</span>";
        echo "<br>";
        echo "</div>";
        echo "</div>";

    } 
    echo $this->Html->link('Aceptar', '/VisitCompanies/index' , [ 'class' => 'button', 'bootstrap-type' => 'primary',
    'class' => 'btn  btn-primary ',
    'rule' => 'button'] ) 

?>


</div>
</div>