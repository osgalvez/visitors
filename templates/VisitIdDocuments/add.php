<!-- File: templates/Articles/add.php -->

<h1>Agregar Identificacion</h1>
<?php
    echo $this->Form->create( $idDocument , ['type' => 'file']);
    // Hard code the user for now.
    echo $this->Form->file('img' , ['capture'=>"camera" , 'accept'=>"image/*"]);
   
    echo $this->Form->control('visit_visitor_id', ['type' => 'hidden', 'value' => $id_visitor]);
    echo $this->Form->button(__('Guardar Archivo'));
    echo $this->Form->end();
?>