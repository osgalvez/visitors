<!-- File: templates/Articles/view.php -->
<div class="section">
    <div class="row">
        <div class="col-md-12">
        <h1><?= h($country->country_name)  ?></h1>
        </div>
        <div class="col-md-12">
        <p>Código de País: <?= h($country->country_code) ?></p>
        </div>
        <div class="col-md-12">
        <p>Status: <?= h($country->status) ?></p>
        </div>
        <div class="col-md-12">
        <p><small>Created: <?= $country->insert_date->format(DATE_RFC850) ?></small></p>
        </div>
        <div class="col-md-12">

        <?= $this->Html->link('Regresar',  
    array('action' => 'index'), 
    array(
        'bootstrap-type' => 'primary',
        'class' => 'btn  btn-primary ',
        // transform link to a button
        'rule' => 'button'
    ) ) ;?>
        </div>
    </div>    <!-- File: templates/Articles/index.php -->
</div>



