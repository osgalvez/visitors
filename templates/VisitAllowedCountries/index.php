<!-- File: templates/Articles/index.php -->
<h1>Paises Permitidos</h1>
<?= $this->Html->link('Agregar', 
    array('action' => 'add'), 
    array(
        'bootstrap-type' => 'primary',
        'class' => 'btn  btn-primary ',
        // transform link to a button
        'rule' => 'button'
    )
); ?>


<table id="document-types" class="display">    
<thead> <tr>
        <th>ID</th>
        <th>Código de Pais</th>
        <th>Pais</th>
        <th>Permitido</th>
        <th>Acción</th>
   

    </tr>
    </thead>
    <!-- Here is where we iterate through our $articles query object, printing out article info -->

    <?php foreach ($allowed_country as $contry): ?>
    <tr>
        <td>
            <?= $this->Html->link($contry->id, ['action' => 'view', $contry->id]) ?>
        </td>
        <td>
            <?= $this->Html->link($contry->country_code, ['action' => 'view', $contry->id]) ?>
        </td>
        <td>
        <?= $this->Html->link($contry->country_name, ['action' => 'view', $contry->id]) ?>
        </td>
        <td>
            <?= $contry->status  == 'Activo' ? 'Si' : 'No' ?>
        </td>
        <td>
            <?php 
            if($contry->status == 'Activo') {
                echo  $this->Html->link("<i  class='fas fa-lock' title='Bloquear Pais'></i> ", ['controller' => 'visit_allowed_countries','action' => 'disable', $contry->id], 
                ['escape' => false] ) ;  
                
            } 
            else {
                echo  $this->Html->link("<i  class='fas fa-lock-open' title='Permitir Pais'></i> ", ['controller' => 'visit_allowed_countries','action' => 'enable', $contry->id], 
                ['escape' => false] ) ;  

            }
            
             ?>
        </td>

      
   
    </tr>
    <?php endforeach; ?>
</table>
