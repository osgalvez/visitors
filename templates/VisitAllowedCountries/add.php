<!-- File: templates/Articles/add.php -->
<div class="container" id="content-inside">
<div id="header" class='row'>
    <div class="col-md-12">
    <h1>Agregar Pais Permitido</h1>
    </div>
</div>
<div id="" class='row'>
    <div class='col-md-12'>
        
    </div>
</div>
<?php
    echo $this->Form->create($visit_allowed_country);
    // Hard code the user for now.
    echo "<div class='row'>";
    echo "<div class='col-md-6'>";
    echo $this->Form->control('country_code', ['label' => 'Código de País' , 'class' => 'form-control']);
    echo "</div>";
    echo "<div class='col-md-6'>";
    echo $this->Form->control('country_name', ['label' => 'País' , 'class' => 'form-control'] );
    echo "</div>";
    echo "</div>";
    echo "<br>";
    echo "<div class='row'>";
    echo "<div class=col-6>";
    echo $this->Form->button('Guardar', ['type' => 'submit','id'=> 'btn-save' , 'class' => 'btn btn-success']);
    echo "</div>";
    echo "</div>";

    echo $this->Form->end();
?>
</div>





