<!-- File: templates/Articles/view.php -->
<div class="section">
    <div class="container"> 
        <div class="row">
            <div class='col-md-4'>
                <h4>Nombre Completo : </h4>
            </div>
            <div class='col-md-4'>
                <span>   <?= $result[0]['contractor_employees']['name'] . " " . $result[0]['contractor_employees']['last_name'] . " " . $result[0]['contractor_employees']['second_last_name']  ?> </span>
            </div>
        </div>
        <div class="row">
            <div class='col-md-4'>
                <h4>Documento : </h4>
            </div>
            <div class='col-md-4'>
                <span>   <?= $result[0]['contractor_document_types']['description']   ?> </span>
            </div>
        </div>
        <div class="row">
            <div class='col-md-4'>
                <h4>Status : </h4>
            </div>
            <div class='col-md-4'>
                <span>   <?php 
                if($result[0]['status'] === 'Registrado') {
                    echo '<i class="warning fas fa-2x fa-exclamation-triangle" alt="Documento En Proceso de Validación"></i> ' .  $result[0]['status'] ;
                }
                if($result[0]['status'] === 'Valido') {
                    echo '<i class="good fa-2x fas fa-check-circle" alt="Documento Valido"></i> ' .  $result[0]['status'] ;
                }  
                if($result[0]['status'] === 'No Valido') {
                    echo '<i class="bad fas fa-2x fa-times-circle" alt="Documento No Valido"></i> ' .  $result[0]['status'] ;
                }  
                
                ?> 
                
                </span>
            </div>
        </div>
        <hr/>
        <div class="row">
            <div class='col-md-4'>
            <?= $this->Html->link('Cancelar', ['controller' => 'contractor-employees', 'action' => 'view', $result[0]['contractor_employee_id']]  , [ 'rule' => 'button','class' => 'btn  btn-primary'] ) ?>  
            </div>
            <div class='col-md-4'>
            <?php 
            
            if($result[0]['editable'] == 1 ) {
                echo $this->Html->link('Cambiar Archivo', ['controller' => 'contractor-employee-documents', 'action' => 'edit', $result[0]['id']]  , [ 'rule' => 'button','class' => 'btn  btn btn-warning'] ) ;

            }
            ?>
                        </div>

        </div>
    </div>
    <hr/>
<?php 

if($result[0]['extension'] == 'application/pdf') {
    echo '<iframe src="data:'.$result[0]['extension'].';base64,'.$result[0]['document_file'].'" width="100%" height="600px" style="border:none;"></iframe>';
}
else {
    echo '<img src="data:'.$result[0]['extension'].';base64, '. $result[0]['document_file'].'"  />' ; 
}
?>
</div>
