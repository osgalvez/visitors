<!-- File: templates/Articles/add.php -->
<div class='container'>
  <div class='section-inside'>
<h4>Cambiar Archivo</h4>
<span class="required-field">(*) Campos requeridos</span>
<div class="card-body">
  <div class="card-text">
    <dl class="row">
      <dt class="col-4">Nombre</dt>
      <dd class="col-8"><?= $result[0]['contractor_employees']['name']." ".$result[0]['contractor_employees']['last_name']." ".$result[0]['contractor_employees']['second_last_name']?></dd>
    </dl>
    <dl class="row">
      <dt class="col-4">Documento</dt>
      <dd class="col-8"><?= $result[0]['contractor_document_types']['description']?></dd>
    </dl>
  </div>
</div>
<?php
 
echo $this->Form->create($contractor_employee_document , ['id' => 'employee_documents', 'type' => 'file']);
        echo '<label for="document_file">Archivo (*) </label>';
        echo $this->Form->file('document_file' , [ 'class' => 'form-control' , 'required' => true]);
        echo $this->Form->control('comments' , ['label' => 'Observaciones','type' => 'text','class' => 'form-control' , 'value' => $result[0]['comments']]);
    echo "<br>";
    echo $this->Form->hidden('contractor_employee_document_id', ["id" => 'contractor_employee_document_id' , 'value' => $document_id]);
 // <button type="submit" class="btn btn-primary col-md-6">Registrar visita</button>
    echo $this->Form->button('Enviar', ['type' => 'submit','id'=> 'btn-save' , 'class' => 'btn btn-success']);
    echo $this->Html->link('Cancelar', '/VisitCompanies/index' , [ 'class' => 'button', 'bootstrap-type' => 'danger',
    'class' => 'btn  btn-danger ',
    'rule' => 'button'] ) ;

    echo $this->Form->end();
?>

</div>
</div>
