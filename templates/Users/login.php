<!-- File: templates/Articles/add.php -->
<div class="container" id="login-form">
    <div class="section">
        <div class="section-inside">
        
<h1>Acceso de Usuario</h1>
<?php
    echo $this->Form->create();
    // Hard code the user for now.
    echo $this->Form->control('username',['label'=> 'Usuario', 'class' => 'form-control']);
    echo $this->Form->control('password', ['label'=> 'Contraseña','type' => 'password' , 'class' => 'form-control']);
    echo $this->Form->button('Aceptar' , ['type' => 'submit' , 'class' => 'btn-success btn' , 'id' => 'login-button']);
    echo $this->Form->end();
?>

</div>
</div>
</div>