<!-- File: templates/Articles/add.php -->
<div class='container'>
  <div class='section-inside'>
<h4>Agregar documento</h4>
<span class="required-field">(*) Campos requeridos</span>

<?php
 
echo $this->Form->create($contractor_employee_document , ['id' => 'employee_documents', 'type' => 'file']);
    // Hard code the user for now.
     echo '<label for="document_type_id">Tipo de Documento (*)</label>';
    echo  $this->Form->select('document_type_id',  $documents_results, [
        'empty' => 'Seleccionar Tipo de Documento' ,'label' => 'Tipo de Documento', 'class' => 'form-control' ,  'required' => true ]);
        echo '<label for="document_file">Archivo (*) </label>';
        echo $this->Form->file('document_file' , [ 'class' => 'form-control' , 'required' => true]);
        echo $this->Form->control('comments' , ['label' => 'Observaciones','type' => 'text','class' => 'form-control']);

  /*  echo '<label for="nationality">Nationality</label>';
    echo $this->Form->select(
        'nationality',
        ['Mexican'=>'Mexican', 'Japanese'=>'Japanese', 'Hungarian'=>'Hungarian', 'North American'=>'North American', 'Other'=>'Other'],
        ['empty' => 'Nationality ...']
    );*/
    echo "<br>";
    echo $this->Form->hidden('company_id', ["id" => 'contractor_employee_id' , 'value' => $company_id]);
 // <button type="submit" class="btn btn-primary col-md-6">Registrar visita</button>
    echo $this->Form->button('Enviar', ['type' => 'submit','id'=> 'btn-save' , 'class' => 'btn btn-success']);
    echo $this->Html->link('Cancelar', '/VisitCompanies/index' , [ 'class' => 'button', 'bootstrap-type' => 'danger',
    'class' => 'btn  btn-danger ',
    'rule' => 'button'] ) ;

    echo $this->Form->end();
?>

</div>
</div>
