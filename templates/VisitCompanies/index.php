<!-- File: templates/Articles/index.php -->
<h1>Compañia</h1>

<table id="visit" class="display">    
        <thead><tr>
        <th>Usuario</th>
        <th>Razon Social</th>
        <th>RFC</th>
        <th>Tipo</th>
        <th>Status</th>
        <th>Acciones</th>
    </tr>
    </thead>

    <!-- Here is where we iterate through our $articles query object, printing out article info -->

    <?php foreach ($companies as $company): ?>
    <tr>
    <td>
    <?=  $this->Html->link($company['id'], ['controller' => 'visit_companies','action' => 'view', $company->id], ['escape' => false] ) ?>
        </td>
        <td>
        <?= $company['name'] ?>
        </td>
        
        <td>
            <?= $company['rfc']?>
        </td>
        <td>
            <?= $company['visit_external_type_id'] ?>
        </td>
        <td>
            <?= $company['status'] ?>
        </td>
        <td>
        <?= $this->Html->link("<i class='fa fa-2x fa-users'  title='Agregar Plantilla' ></i>", ['controller'=>'contractor_employees' , 'action' => 'add', $company['id'] ], ['escape' => false] ) ?>
        <?= $this->Html->link("<i class='fa fa-2x  fa-city'  title='Agregar Documento' ></i>", ['controller'=>'contractor-company-documents' , 'action' => 'add', $company['id'] ], ['escape' => false] ) ?>

        </td>
        
    </tr>
    <?php endforeach; ?>
</table>

<?php if(count($document_list  ) > 0 ) { ?> 

<hr>
<div class='container'> 
<div class='section'>
<div class='section-inside'>

<h1>Documentos de Compañia</h1>

<table id="visit" class="display">    
        <thead><tr>
        <th>Tipo de Documento </th>
        <th>Status</th>
        <th>Ver Documento</th>
    </tr>
    </thead>

<?php 

foreach($document_list as $key=> $value ){
    $icono_status = "";
    $icono_editable="";
    if($value['status'] === 'Registrado') {
        $icono_status = "<i class='warning fas fa-exclamation-triangle' title ='Documento en Proceso de Validacion' ></i> ";
    }
    if($value['status'] === 'Valido') {
        $icono_status = "<i class='good fas fa-check-circle' title ='Documento Válido'></i>  ";

    }
    if($value['status'] === 'No Valido') {
        $icono_status = " <i class='bad fas fa-times-circle' title ='Documento No Válido'></i> ";

    }
    if($value['editable'] == 1 ) {
        
       // $icono_editable = $this->Html->link('<i class="fas fa-2x fa-edit" title = "Editar Documento"></i>', ['controller'=>'contractor_employee_documents' , 'action' => 'edit', $value['id'] ], ['escape' => false] ) ;

    } 
    echo "<tr>";
    echo "<td>".$value['contractor_document_types']['description']."</td>";
    echo "<td style='text-align: center;'>".$icono_status."</td>";
    echo "<td>";
    echo  $this->Html->link('<i class="fas fa-2x fa-eye" title = "Ver Documento" ></i>', ['controller'=>'contractor_company_documents' , 'action' => 'view', $value['id'] ], ['escape' => false] ) ;
    echo $icono_editable;
    echo "</td>";
    echo '</tr>';
}
} ?>

</table>
</div>
</div>
</div>