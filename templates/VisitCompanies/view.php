<!-- File: templates/Articles/view.php -->
<div class='container'>
  <div class='section-inside'>
  <h3><?= h($company[0]['id']) ." - ". h($company[0]['name'])  ?></h3>
  <h4>RFC: <?= h($company[0]['rfc']) ?> </h4>

<?php 
 echo  $this->Html->link("<i class='fas fa-2x fa-plus-circle' title='Agregar Personal'></i><span class='add-side-span'>Agregar Personal</span>", 
 ['controller' => 'contractor_employees','action' => 'add',$company[0]['id']], ['escape' => false , 'class' => 'add-side'] );
 
if(count($employees_list) > 0 ) {
    echo "<div><h4>Plantilla </h4>";
   

    echo "<table id='contractor-employee' class='display'><thead>"  ;
            echo "<tr>";
            echo "<th>Nombre</th>";
            echo"<th>Numero IMSS</th>";
            echo"<th>Status</th>";
            echo"<th>Fecha de Alta</th>";
            echo"<th>Acción</th>";
            echo"</tr></thead>";
    foreach($employees_list as $key=> $value ){
        echo "<tr>";
         echo "<td>".$this->Html->link($value['name']." ".$value['last_name']." ".$value['second_last_name'], ['controller'=> 'contractor_employees' , 'action' => 'view', $value['id']]) ."</td>";
        echo "<td>$value[ssn]</td>";
        echo "<td>$value[status]</td>";
        echo "<td>".$value['insert_date']->i18nFormat('dd-MM-yyyy HH:mm:ss')."</td>";

        echo "<td>";
        echo  $this->Html->link("<i class='fas fa-file-medical' title='Agregar Documento'></i><span class='add-side-span'>Subir Documento</span>",  ['controller' => 'contractor_employee_documents','action' => 'add', $value['id']], ['escape' => false , 'class' => 'add-side'] );
        echo  $this->Html->link("<i class='fas fa-clipboard-check' title='Aplicar Examen'></i><span class='add-side-span'>Aplicar Examen</span>",  ['controller' => 'contractor_employees','action' => 'take_test', $value['id']], ['escape' => false , 'class' => 'add-side'] );
        if($value['status'] == 'Activo') {
                echo  $this->Html->link("<i class='fas fa-qrcode' title='Generar QR'></i><span class='add-side-span'>Generar QR</span>", ['controller'=>'contractor_employees' , 'action' => 'generate_qr', $value['id'] ], ['escape' => false, 'class' => 'add-side'] ) ;
            }
        echo "</td>";
        echo "</tr>";
    }
}
    echo '</table></div>';
    echo $this->Html->link('Regresar a la lista', '/VisitCompanies/index' , [ 'class' => 'button', 'bootstrap-type' => 'primary',
        'class' => 'btn  btn-primary ',
        'rule' => 'button'] ) ;?>


</div>
</div>
