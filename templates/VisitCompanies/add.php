<!-- File: templates/Articles/add.php -->

<h1>Alta de Compañia</h1>
<?php
    echo $this->Form->create($company );
    // Hard code the user for now.
//    echo  $this->Form->select('visit_visitor_id', $visitor_result, [
  //      'empty' => 'Visitor' ,'default' => $visitorid]);
    echo $this->Form->control('fiscal_name', ['label' => 'Razón Social', 'type' => 'text' , 'class' => 'form-control']);
    echo $this->Form->control('common_name' , ['label' => 'Nombre Comercial','type' => 'text','class' => 'form-control']);
    echo $this->Form->control('rfc' , ['label' => 'RFC', 'class' => 'form-control']);
    echo '<label for="type">Tipo </label>';    
    echo $this->Form->select(
        'type' , 
        ['A'=>'A', 'B'=>'B', 'C'=>'C', 'Other'=>'Other'],
      ['empty' => 'Tipo  ...' , 'label' => 'Tipo' ,'class' => 'form-control' ]);
  /*  echo '<label for="nationality">Nationality</label>';
    echo $this->Form->select(
        'nationality',
        ['Mexican'=>'Mexican', 'Japanese'=>'Japanese', 'Hungarian'=>'Hungarian', 'North American'=>'North American', 'Other'=>'Other'],
        ['empty' => 'Nationality ...']
    );*/
                       // <button type="submit" class="btn btn-primary col-md-6">Registrar visita</button>
    echo $this->Form->button('Guardar', ['type' => 'submit','id'=> 'btn-save' , 'class' => 'btn btn-success']);
    echo $this->Form->end();
?>

