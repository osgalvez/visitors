<?php
// src/Model/Entity/Article.php
namespace App\Model\Entity;

use Cake\ORM\Entity;

class ContractorCompanyDocument extends Entity
{
    protected $_accessible = [
        '*' => true,
        'id' => true,
        'visit_company_id' => true,
        'contractor_document_type_id' => true,
        'name' => true,
        'extension' => true

    ];
}