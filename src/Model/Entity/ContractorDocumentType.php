<?php
// src/Model/Entity/Article.php
namespace App\Model\Entity;

use Cake\ORM\Entity;

class ContractorDocumentType extends Entity
{
    protected $_accessible = [
        '*' => true,
        'id' => true,
        'description' => true,
        'prefix' => true
    ];
}