<?php
// src/Model/Entity/Article.php
namespace App\Model\Entity;
use Cake\ORM\Entity;

class VisitCompany extends Entity
{
    protected $_accessible = [
        '*' => true,
        'id' => true,
        'rfc' => true,
        'fiscal_name' => true,
        'common_name' => true
        
    ];
}