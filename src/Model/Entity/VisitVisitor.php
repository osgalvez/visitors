<?php
// src/Model/Entity/Article.php
namespace App\Model\Entity;

use Cake\ORM\Entity;

class VisitVisitor extends Entity
{
    protected $_accessible = [
        '*' => true,
        'id' => true,
        'fist_name' => true,
        'last_name' => true,
        'second_last_name' => true
    ];
}