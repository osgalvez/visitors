<?php
// src/Model/Entity/Article.php
namespace App\Model\Entity;

use Cake\ORM\Entity;

class VisitVisitorVisit extends Entity
{
    protected $_accessible = [
        '*' => true,
        'id' => true,
        'reason' => true,
        'visited_person' => true,
        'area' => true,
        'entry_date' => true,
        'status' => true,
        'exit_date' => true
    ];
}