<?php
// src/Model/Entity/Article.php
namespace App\Model\Entity;

use Cake\ORM\Entity;

class VisitAllowedCountry extends Entity
{
    protected $_accessible = [
        '*' => true,
        'id' => true,
        'country_name' => true,
        'code' => true
    ];
}