<?php
// src/Model/Entity/Article.php
namespace App\Model\Entity;

use Cake\ORM\Entity;

class VisitIdDocument extends Entity
{
    protected $_accessible = [
        '*' => true,
        'id' => true,
        'visit_visitor_id' => true,
        'image' => true    ];
}