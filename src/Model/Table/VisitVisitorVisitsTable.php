<?php
// src/Model/Table/ArticlesTable.php
namespace App\Model\Table;

use Cake\ORM\Table;

class VisitVisitorVisitsTable extends Table
{
    public function initialize(array $config): void
    {
        $this->addBehavior('Timestamp');
    }
    public function getVisitsData() {
        /*
        
      $this->loadComponent('Paginator');
        $visitors = $this->paginate($this->VisitVisitors->find()
            ->select(['VisitVisitors.id', 'VisitVisitors.first_name' , 'VisitVisitors.last_name', 'VisitVisitors.second_last_name',
            'VisitVisitors.phone' , 'VisitVisitors.email' , 'VisitVisitors.company' , 'VisitVisitors.other_country' , 'visit_allowed_countries.country_name']) 
            ->join([
            'table' => 'visit_allowed_countries',
            'type' => 'LEFT',
            'conditions' => 'VisitVisitors.nationality = visit_allowed_countries.id',]) );
       
        $this->set(compact('visitors'));
        
        */ 
      $visitors_result =   $this->find('all', array('recursive' => -1) )
      ->select([
          'VisitVisitorVisits.id' , 'VisitVisitorVisits.visit_visitor_id' , 'VisitVisitorVisits.reason', 'VisitVisitorVisits.visited_person', 'VisitVisitorVisits.employee_number' 
          , 'VisitVisitorVisits.area_code' , 'VisitVisitorVisits.status', 'VisitVisitorVisits.area', 'VisitVisitorVisits.entry_date' , 'VisitVisitorVisits.exit_date',
          'VisitVisitorVisits.approved_exit', 'visit_visitors.first_name', 'visit_visitors.last_name', 'visit_visitors.second_last_name' 
        ])
        ->join([
            'table' => 'visit_visitors',
            'type' => 'LEFT',
            'conditions' => 'VisitVisitorVisits.visit_visitor_id = visit_visitors.id'
        ],) 
              ->where([ "VisitVisitorVisits.status = 'active'"]);

      return ($visitors_result);
    }

    public function getRelatedVisits($visit_id = null , $approver = '' , $date_time = null ) {
    $base_visit = $this->findById($visit_id)->firstOrFail();
    $updated = $this->updateAll( array('approved_exit' => 1 , 'approved_exit_by' => $approver , 'approved_exit_datetime' => $date_time , 'update_user_id' => $approver , 'update_date_time' => $date_time),
      array('status' => 'active' , "approved_exit = 0 " , 'visit_visitor_id = '.$base_visit->visit_visitor_id , "convert(date,entry_date) =  '".$base_visit->entry_date->format('Y-m-d')."'")
  );
    
    return ($updated);
  }

  public function getIdsRelatedVisits($visit_id= null ) {
    $base_visit = $this->findById($visit_id)->firstOrFail();
    $ids_result = $this->find('list' , array('recursive' => -1) )
    ->select([
      'id' 
    ])
    ->where(array('status' => 'active' , "approved_exit = 1 " , 'visit_visitor_id = '.$base_visit->visit_visitor_id , "convert(date,entry_date) =  '".$base_visit->entry_date->format('Y-m-d')."'")); 
    return ($ids_result->toList());
  }

  public function getApprovedExitsRelated($visit_id = null , $approver = '' , $date_time = null ) {
    $base_visit = $this->findById($visit_id)->firstOrFail();
    $updated = $this->updateAll( array('status' => 'inactive' , 'exit_date' => $date_time , 'update_user_id' => $approver , 'update_date_time' => $date_time),
    array('status' => 'active' , "approved_exit = 1 " , 'visit_visitor_id = '.$base_visit->visit_visitor_id , "convert(date,entry_date) =  '".$base_visit->entry_date->format('Y-m-d')."'")
);


 

  }

  public function getMyVisits($user_id = null) {
    /*
    order(['FirstComment.created' =>'ASC'])
  $this->loadComponent('Paginator');
    $visitors = $this->paginate($this->VisitVisitors->find()
        ->select(['VisitVisitors.id', 'VisitVisitors.first_name' , 'VisitVisitors.last_name', 'VisitVisitors.second_last_name',
        'VisitVisitors.phone' , 'VisitVisitors.email' , 'VisitVisitors.company' , 'VisitVisitors.other_country' , 'visit_allowed_countries.country_name']) 
        ->join([
        'table' => 'visit_allowed_countries',
        'type' => 'LEFT',
        'conditions' => 'VisitVisitors.nationality = visit_allowed_countries.id',]) );
   
    $this->set(compact('visitors'));
    
    */ 
  $visitors_result =   $this->find( 'all',    array('recursive' => -1) )
  ->select([
      'VisitVisitorVisits.id' , 'VisitVisitorVisits.visit_visitor_id' , 'VisitVisitorVisits.reason', 'VisitVisitorVisits.visited_person', 'VisitVisitorVisits.employee_number' 
      , 'VisitVisitorVisits.area_code' , 'VisitVisitorVisits.status', 'VisitVisitorVisits.area', 'VisitVisitorVisits.entry_date' , 'VisitVisitorVisits.exit_date',
      'VisitVisitorVisits.approved_exit', 'visit_visitors.first_name', 'visit_visitors.last_name', 'visit_visitors.second_last_name' 
    ])
    ->join([
        'table' => 'visit_visitors',
        'type' => 'LEFT',
        'conditions' => 'VisitVisitorVisits.visit_visitor_id = visit_visitors.id'
    ],)
    ->where(["VisitVisitorVisits.employee_number = ".$user_id , "VisitVisitorVisits.status = 'active'"]);

  return ($visitors_result);
}

}