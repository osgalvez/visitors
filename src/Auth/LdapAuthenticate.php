<?php

namespace Cake\Auth;

use Cake\Auth\BaseAuthenticate;
use Cake\Http\ServerRequest;
use Cake\Http\Response;

class LdapAuthenticate extends BaseAuthenticate {

    protected $_host = '10.6.1.3' ;

    public function authenticate(ServerRequest $request, Response $response) {
        $username = $request->data['user'] ;
        $password = $request->data['password'] ;
        $ds = @ldap_connect($this->_host) ;
        if (!$ds) {
            throw \Cake\Error\FatalErrorException ('Unable to connect to LDAP host.') ;
        }
        $basedn = "OU=Users,OU=ZMX,DC=zoltek,DC=local";
        $dn = "uid=$username, ".$basedn;
        $ldapbind = @ldap_bind($ds, $dn, $password);
        if (!$ldapbind) {
            return false ;
        }
        // Do whatever you want with your LDAP connection... 
        $entry = ldap_first_entry ($ldapbind) ;
        $attrs = ldap_get_attributes ($ldapbind, $entry) ;
        $user  = [] ;
        // Loop
        for ($i = 0 ; $i < $attrs["count"] ; $i++) {
            $user[$attrs[$i]] = ldap_values ($ldapbind, $entry, $attrs[$i])[0] ;
        }
        // Then close it and return the authenticated user
        ldap_unbind ($ldapbind) ;
        ldap_close ($ldapbind);
        return true ;
    }

}

?>