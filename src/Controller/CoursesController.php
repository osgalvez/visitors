<?php
// src/Controller/ArticlesController.php
namespace App\Controller;
use Cake\ORM\TableRegistry;

class CoursesController extends AppController
{
    public function initialize(): void
    {
        parent::initialize();

        $this->loadComponent('Paginator');
        $this->loadComponent('Flash'); // Include the FlashComponent
        
    }
    public function index()
    {   
        /*$session = $this->getRequest()->getSession();
        $company = $session->read('UserInfo.CompanyID');
        $visitors = $this->ContractorEmployees->find('all')->where(["visit_company_id" => $company]);
        $this->set(compact('visitors'));*/
    }
    public function view($id = null ) {
        $video_file = '';
       /* $employee_documents = TableRegistry::getTableLocator()->get('ContractorEmployeeDocuments');
        $query = $employee_documents->find('all')->select(["ContractorEmployeeDocuments.id" , 
        "ContractorEmployeeDocuments.contractor_document_type_id", "ContractorEmployeeDocuments.name", "ContractorEmployeeDocuments.extension",
        "ContractorEmployeeDocuments.document_file" , "ContractorEmployeeDocuments.status", "contractor_document_types.description"])
        ->join([
            'table' => 'contractor_document_types',
            'type' => 'LEFT',
            'conditions' => 'ContractorEmployeeDocuments.contractor_document_type_id = contractor_document_types.id']) 
        ->where(['ContractorEmployeeDocuments.contractor_employee_id' => $id]);
        $query->enableHydration(false); // Results as arrays instead of entities
        $result = $query->toList();
     
        $employee = $this->ContractorEmployees->findById($id)->firstOrFail();
        $this->set(compact('employee','result')); */
        if($id == 1 ) {
            $video_file = 'induccion_ehs.mp4';
            $title = "Inducción de Seguridad, Salud y Medio Ambiente.";
        }
        if($id == 2 ) {
            $title = "Curso Trabajar Seguro al Conducir.";
            $video_file = 'seguridad_al_manejar.mp4';
        }
        $this->set('video_file', $video_file);
        $this->set('title', $title);
    }

    public function add($company_id = null){
        $contractor_employee = $this->ContractorEmployees->newEmptyEntity();
        $black_list = TableRegistry::getTableLocator()->get('VisitBlackListMembers');

        if($this->request->is('post')) {
            $data = $this->request->getData();
            foreach($data as $key=>$element ) {
                if (is_string($element)){
                    $element = trim($element);    
                    $data[$key] = $element;
                }
            }
            $first = $data['name'];
            $last_name = $data['last_name'];
            $second_last_name = $data['second_last_name'];
            $ssn = $data['ssn'];
          /*  $members = $black_list->find()->where(['name =' => $first , 'last_name =' => $last_name, 'second_last_name = ' => $second_last_name , 'ssn' => $ssn] )->toList();
          if(!empty($members)){
            $this->Flash->error(__('No se puede agregar al Usuario'));
            return $this->redirect(['action' => 'add']);
          }*/
          
            $contractor_employee = $this->ContractorEmployees->patchEntity($contractor_employee , $this->request->getData());
            $contractor_employee->status = 'Registrado';
            if($this->ContractorEmployees->save($contractor_employee)) {
                $this->Flash->success(__('Persona Agregada a la Plantilla'));
                $inserted_id =  $contractor_employee->id ;
                return $this->redirect(['controller'=>'contractor_employee_documents','action' => 'add' ,  $inserted_id]);
            }
            $this->Flash->error(__('Unable to add your visitor.'));
        }

                $this->set('contractor_employee', $contractor_employee);
                $this->set('company_id', $company_id);

    }
}

