<?php
// src/Controller/ArticlesController.php
namespace App\Controller;
use Cake\I18n\FrozenTime;
use Cake\ORM\TableRegistry;
use Cake\Datasource\ConnectionManager;
use DateTime;


class VisitVisitorVisitsController extends AppController
{
    public function initialize(): void
    {
        parent::initialize();
        
        ConnectionManager::setConfig('zoltekmx', [
            'className' => 'Cake\Database\Connection',
            'driver' => 'Cake\Database\Driver\Mysql',
            'persistent' => false,
            'host' => '209.50.52.248',
            'username' => 'zoltek',
            'password' => 'Yhzt3_53',
            'database' => 'zoltek_visitors',
            'encoding' => 'utf8',
            'timezone' => 'UTC',
            'cacheMetadata' => true,
        ]);

        ConnectionManager::setConfig('Fortia', [
            'className' => 'Cake\Database\Connection',
            'driver' => 'Cake\Database\Driver\Sqlserver',
            'persistent' => false,
            'host' => 'ZMXPM-SQL',
            'username' => 'sa',
            'password' => 'BDS3rv3r',
            'database' => 'Fortia',
            'timezone' => 'UTC',
        ]);

        $this->loadComponent('Paginator');
        $this->loadComponent('Flash'); // Include the FlashComponent
        $this->Authentication->allowUnauthenticated(['view', 'index']);
    }
    public function index()
    {
        $visits = $this->VisitVisitorVisits->getVisitsData();
        $visits = $visits->toList();
        $this->set(compact('visits'));
    }

    public function view($id = null ) {
        $visitor = $this->VisitVisitorVisits->findById($id)->firstOrFail();
        $this->set(compact('visitor'));
    }

    public function visitExit($id= null ){
        $session = $this->getRequest()->getSession(); 
        $person_id =  $session->read('UserInfo.UserID');
        $now = FrozenTime::now();
        $idToDelete = $this->VisitVisitorVisits->getIdsRelatedVisits($id);
        $related_visits = $this->VisitVisitorVisits->getApprovedExitsRelated($id , $person_id , $now);
        $connection_mysql = ConnectionManager::get('zoltekmx');
  
        foreach($idToDelete as $key => $value) {
            $result = $connection_mysql->delete('visits', ['visit_id' => $value]);
        
        }
       $this->Flash->success(__('Salida registrada'));
       return $this->redirect(['action' => 'index']);
    
    }

public function getVisits(  ){
   
    //$data_entry = $this->request->getData();
    
    $connection_Fortia = ConnectionManager::get('Fortia');
    $filter = "'%".$_GET['term']."%'";
    $results = $connection_Fortia->execute("SELECT CONCAT(Name,' - ',Area) as label, Name as value from viEmpleados_zoltek where Name like $filter and Status ='A'")->fetchAll('assoc');
    print_r( json_encode($results));
    exit;    
}

public function get_person_info($person) {
    $connection_Fortia = ConnectionManager::get('Fortia');
    $results = $connection_Fortia->execute("SELECT EmployeeNumber as EmployeeNumber , AreaCode , Area from viEmpleados_zoltek where Name = '$person' and Status ='A'")->fetchAll('assoc');
    return($results);

}

public function myVisits () {
    $session = $this->getRequest()->getSession(); 
    $person_id =  $session->read('UserInfo.UserID');
    $my_visits = $this->VisitVisitorVisits->getMyVisits($person_id);
    $this->set(compact('my_visits'));
}

public function approveExit($visit_id = null ) {
    $session = $this->getRequest()->getSession(); 
    $person_id =  $session->read('UserInfo.UserID');
    $now = FrozenTime::now();
    $related_visits = $this->VisitVisitorVisits->getRelatedVisits($visit_id , $person_id , $now);
   $this->Flash->success(__('Salida aprobada'));
   return $this->redirect(['action' => 'my-visits']);
}


public function askAditionalVisit () {
}


public function addAnotherVisit()
{
    $visit = $this->VisitVisitorVisits->newEmptyEntity();
    
    
    if($this->request->is('post')) {
        $session = $this->getRequest()->getSession();
        $visit_data = unserialize($session->read('Visitor.Data'));
    
        $enc_visit_data = unserialize($session->read('EncVisitor.Data'));
        $data_entry = $this->request->getData();
         $visit = $this->VisitVisitorVisits->patchEntity($visit , $data_entry);
        $now = FrozenTime::now();
        $employee_id = $this->get_person_info($data_entry['visited_person']);
       
            $ciphering = "AES-128-CTR"; 
            // Use OpenSSl Encryption method 
            $iv_length = openssl_cipher_iv_length($ciphering); 
            $options = 0;
            // Non-NULL Initialization Vector for encryption 
            $encryption_iv = '1234567891011121'; 
            // Store the encryption key 
            $encryption_key = "ZTKMX-0022"; 
            // Use openssl_encrypt() function to encrypt the data 
            
            $visited_person_encrypted = openssl_encrypt($data_entry['visited_person'], $ciphering, 
            $encryption_key, $options, $encryption_iv); 
            $visited_area_encrypted = openssl_encrypt($employee_id[0]['Area'], $ciphering, 
            $encryption_key, $options, $encryption_iv); 
            
            $enc_visit_data['visited_person'] = $visited_person_encrypted;
            $enc_visit_data['visited_area'] = $visited_area_encrypted;
            $enc_visit_data['entry_date'] = $now;
            $enc_visit_data['status'] = 'active';

            // Display the encrypted string 
           
            
            /* Non-NULL Initialization Vector for decryption 
            $decryption_iv = '1234567891011121'; 
            
            // Store the decryption key 
            $decryption_key = "ZTKMX-0022"; 
            
            // Use openssl_decrypt() function to decrypt the data 
            $decryption=openssl_decrypt ($encryption, $ciphering,  
                    $decryption_key, $options, $decryption_iv); 
                    Display the decrypted string 
             echo "Decrypted String: " . $decryption; 
            */
           
       $visit ->visited_person = $data_entry['visited_person'];
        $visit->employee_number  = $employee_id[0]['EmployeeNumber'];
        $visit->area = $employee_id[0]['Area'];
        $visit->area_code = $employee_id[0]['AreaCode'];
        $visit->entry_date = $now ;
        $visit->status = 'active';
        $visit->approved_exit = 0;
        $connection_mysql = ConnectionManager::get('zoltekmx');
        $visit->visit_visitor_id = $visit_data->visit_visitor_id;
        $visit->terms_accepted =         $visit_data->terms_accepted ;
        $visit->signature = $visit_data->signature;
        if($this->VisitVisitorVisits->save($visit)) {
            $this->Flash->success(__('Visita Registrada'));
            $enc_visit_data['visit_id'] = $visit->id;
            $connection_mysql->insert('visits', $enc_visit_data
                , ['entry_date' => 'datetime']);
                $session->write('Visitor.Data', serialize($visit));
            return $this->redirect(['action' => 'ask_aditional_visit']);
        }
        $this->Flash->error(__('No se pudo agregar la visita.'));
    }
    
    $this->set('visit', $visit);
}
    public function add($visitorid = ''){
        $visitors = TableRegistry::getTableLocator()->get('VisitVisitors');
        $query = $visitors->find();
        $session = $this->getRequest()->getSession(); 

        $query->enableHydration(false); // Results as arrays instead of entities
        $result = $query->toList(); 
        foreach($result as $visitor => $data) {
            $visitor_result[$data['id']] = $data['first_name']. " " . $data['last_name']." " . $data['second_last_name']. " - " . $data['company'];   
        }
        // Execute the query and return the array
        $this->set(compact('visitor_result', 'visitorid'));

        $visit = $this->VisitVisitorVisits->newEmptyEntity();
        $visit = $this->VisitVisitorVisits->patchEntity($visit , $this->request->getData());
        $now = FrozenTime::now();
        $employee_id = $this->get_person_info($visit->visited_person);
        if($this->request->is('post')) {
            $data_entry = $this->request->getData();
            $visitor_id = $data_entry['visit_visitor_id'];
           
                $ciphering = "AES-128-CTR"; 
                // Use OpenSSl Encryption method 
                $iv_length = openssl_cipher_iv_length($ciphering); 
                $options = 0;
                // Non-NULL Initialization Vector for encryption 
                $encryption_iv = '1234567891011121'; 
                // Store the encryption key 
                $encryption_key = "ZTKMX-0022"; 
                // Use openssl_encrypt() function to encrypt the data 
                $visitor_name_encrypted = openssl_encrypt($visitor_result[ $visitor_id ], $ciphering, 
                            $encryption_key, $options, $encryption_iv); 
                $visited_person_encrypted = openssl_encrypt($data_entry['visited_person'], $ciphering, 
                $encryption_key, $options, $encryption_iv); 
                $visited_area_encrypted = openssl_encrypt($employee_id[0]['Area'], $ciphering, 
                $encryption_key, $options, $encryption_iv); 
                
                // Display the encrypted string 
               
                
                /* Non-NULL Initialization Vector for decryption 
                $decryption_iv = '1234567891011121'; 
                
                // Store the decryption key 
                $decryption_key = "ZTKMX-0022"; 
                
                // Use openssl_decrypt() function to decrypt the data 
                $decryption=openssl_decrypt ($encryption, $ciphering,  
                        $decryption_key, $options, $decryption_iv); 
                        Display the decrypted string 
                 echo "Decrypted String: " . $decryption; 
                */
               
           
            $visit->employee_number  = $employee_id[0]['EmployeeNumber'];
            $visit->area = $employee_id[0]['Area'];
            $visit->area_code = $employee_id[0]['AreaCode'];
            $visit->entry_date = $now ;
            $visit->status = 'active';
            $visit->approved_exit = 0;
            $visit->insert_date_time = $now ;
            $person_id =  $session->read('UserInfo.UserID');
            $visit->insert_user_id = $person_id ;
            $connection_mysql = ConnectionManager::get('zoltekmx');
            


            if($this->VisitVisitorVisits->save($visit)) {
                $this->Flash->success(__('Visita Registrada'));
                $connection_mysql->insert('visits', [
                    'visitor' => $visitor_name_encrypted,
                    'visited_person' => $visited_person_encrypted,
                    'visited_area' => $visited_area_encrypted,
                    'entry_date' => $now,
                    'status' => 'active',
                    'visit_id' => $visit->id
                    ], ['entry_date' => 'datetime']);
                    $session->write('Visitor.Data', serialize($visit));
                    $session->write('EncVisitor.Data', serialize( [
                        'visitor' => $visitor_name_encrypted,
                        'visited_person' => $visited_person_encrypted,
                        'visited_area' => $visited_area_encrypted,
                        'entry_date' => $now,
                        'status' => 'active',
                        'visit_id' => $visit->id,
                         'entry_date' => 'datetime']));
                return $this->redirect(['action' => 'ask_aditional_visit']);
            }
            $this->Flash->error(__('No se pudo agregar la visita.'));
        }

                $this->set('visit', $visit);
    }

    public function getCloudData() {
        $connection_mysql = ConnectionManager::get('zoltekmx');
        $results = $connection_mysql->execute('select visitor, group_concat(visited_person) as visited_person , group_concat(visited_area) as areas,entry_date   from visits  where status= "active" group by visitor , convert(entry_date ,date )')->fetchAll('assoc');
        $ciphering = "AES-128-CTR"; 
                // Use OpenSSl Encryption method 
                $iv_length = openssl_cipher_iv_length($ciphering); 
                $options = 0;
                // Non-NULL Initialization Vector for encryption 
                $decryption_iv = '1234567891011121'; 
                
                // Store the decryption key 
                $decryption_key = "ZTKMX-0022";  
                $total_visits = count($results);
                $decripted_data ;
        foreach($results as $key => $values ) {
            // Display the encrypted string 
               
            
                // Use openssl_decrypt() function to decrypt the data 
                $decripted_visitor=openssl_decrypt ($values['visitor'], $ciphering, $decryption_key, $options, $decryption_iv); 
                $decripted_data[$key]['visitor'] =  $decripted_visitor; 
                 $decripted_person =openssl_decrypt ($values['visited_person'], $ciphering, $decryption_key, $options, $decryption_iv); 
                 $decripted_data[$key]['person'] =  $decripted_person; 

                 $person_explode = explode(",",$values['visited_person']);
                 $persons_decrypted = array();
                 foreach($person_explode as $person ){
                    $decripted=openssl_decrypt ($person, $ciphering, $decryption_key, $options, $decryption_iv); 
                    $persons_decrypted[]  = $decripted ;
                 }
                 $decripted_data[$key]['person'] =  implode(' , ' , $persons_decrypted); 



                 $areas_explode = explode(",",$values['areas']);
                 $areas_decripted = array();
                 foreach($areas_explode as $area ){
                    $decripted=openssl_decrypt ($area, $ciphering, $decryption_key, $options, $decryption_iv); 
                    $areas_decripted[]  = $decripted ;
                 }
                 $decripted_data[$key]['areas'] =  implode(' , ' , $areas_decripted); 
                 $decripted_data[$key]['entry_date'] =   $values['entry_date'];
        } 

        $this->set('decripted_data', $decripted_data);
        $this->set('total_visits', $total_visits);


       
    } 
}