<?php
// src/Controller/ArticlesController.php
namespace App\Controller;
use Cake\ORM\TableRegistry;
use Cake\Datasource\ConnectionManager;
use Authentication\PasswordHasher\DefaultPasswordHasher;

class UsersController extends AppController {
    public function beforeFilter(\Cake\Event\EventInterface $event)
{
    parent::beforeFilter($event);
    $this->Authentication->allowUnauthenticated(['login']);
    $this->Authentication->allowUnauthenticated(['changepassword']);
    $this->Authentication->addUnauthenticatedActions(['login']);
}

public function changepassword() {
    $user = $this->Users->newEmptyEntity();
       

    // Execute the query and return the array

    if($this->request->is('post')) {
        $data_user =  $this->request->getData();
        $hasher = new DefaultPasswordHasher();
        $user ->password =  $hasher->hash($data_user['password']);
        $session = $this->getRequest()->getSession(); 
        $user->username = $data_user['username'];
        if( $this->Users->save($user)) {
            $this->Flash->success(__('Pais Agregado a la Lista. '));
          //  $inserted_id =  $contractor_employee->id ;
          return $this->redirect(['controller'=>'users','action' => 'login']);
        }
       $this->Flash->error(__('No se pudo agregar el pais.'));
    }
      $this->set('user', $user);

}

    public function login()
    {   

        
        $result = $this->Authentication->getResult($this->request->getData());
        // If the user is logged in send them away.
      
        if ($result->isValid()) {
            $session = $this->getRequest()->getSession();
            $identity = $this->Authentication->getIdentity();
           $logged_user_name = $identity->get('username');
           $password = $identity->get('password');

           $session->write('UserInfo.CompanyID' , $logged_user_name);
           $session->write('UserInfo.Key' , $password);

            $target = '/visit-companies/index';
            if($logged_user_name == 'admin') {
                $target = '/head-count/index';
            }
            return $this->redirect($target);
        }
        if ($this->request->is('post') && !$result->isValid()) {
           
            $this->Flash->error('Nombre de Usuario y/o Contraseña No Validos');
        }
    }
    public function logout()
{   

    $session = $this->getRequest()->getSession();
    $session->write('UserInfo.CompanyID', '');
    $session->write('UserInfo.Key', '');
    $this->Authentication->logout();
    return $this->redirect(['controller' => 'Users', 'action' => 'login']);
}


}

?>