<?php
// src/Controller/ArticlesController.php
namespace App\Controller;
use Cake\ORM\TableRegistry;
use Cake\I18n\FrozenTime;
use Authentication\PasswordHasher\DefaultPasswordHasher;
use Cake\Utility\Hash;
use Cake\Utility\Security;
use Cake\Datasource\ConnectionManager;


class ContractorEmployeeDocumentsController extends AppController
{   
    public $allowed_files ;
    public function initialize(): void
    {
        parent::initialize();
        $this->allowed_files =  array('image/jpeg', 'image/png' , 'application/pdf');
        $this->loadComponent('Paginator');
        $this->loadComponent('Flash'); // Include the FlashComponent
    }
    public function index()
    {
        $this->loadComponent('Paginator');
        $contractor_document = $this->Paginator->paginate($this->ContractorEmployeeDocuments->find());
        $this->set(compact('contractor_document'));
    }

    public function view($id = null ) {
         

        $session = $this->getRequest()->getSession();
        $encryption_key = $session->read('UserInfo.Key');

        $query = $this->ContractorEmployeeDocuments->find('all')->select(["ContractorEmployeeDocuments.id" , "ContractorEmployeeDocuments.contractor_employee_id",
        "ContractorEmployeeDocuments.contractor_document_type_id", "ContractorEmployeeDocuments.document_file", "ContractorEmployeeDocuments.extension",
         "ContractorEmployeeDocuments.status", "contractor_document_types.description" , "contractor_employees.name" , "contractor_employees.last_name" , 
         "contractor_employees.second_last_name" , "ContractorEmployeeDocuments.editable"
         ])
        ->join([
            'table' => 'contractor_document_types',
            'type' => 'LEFT',
            'conditions' => 'ContractorEmployeeDocuments.contractor_document_type_id = contractor_document_types.id'
        ])
        ->join([
            'table' => 'contractor_employees',
            'type' => 'LEFT',
            'conditions' => 'ContractorEmployeeDocuments.contractor_employee_id = contractor_employees.id'
        ])
        ->where(['ContractorEmployeeDocuments.id' => $id]);
        $query->enableHydration(false); // Results as arrays instead of entities
        $result = $query->toList();
        
        if($result[0]['document_file'] == NULL || $result[0]['extension']  == NULL ) {
            
            $employee_id = $result[0]['contractor_employee_id'];
            $this->Flash->error(__('El Archivo se ha sincronizado a la red interna de Zoltek, no puede visualizarlo por el momento.'));
            return $this->redirect(['controller'=>'contractor_employees','action' => 'view', $employee_id]);

        }
        $result[0]['document_file'] = Security::decrypt(base64_decode($result[0]['document_file']), $encryption_key);
        $result[0]['extension'] = Security::decrypt(base64_decode($result[0]['extension']), $encryption_key);
        
        $result[0]['contractor_employees']['name'] = Security::decrypt(base64_decode($result[0]['contractor_employees']['name']), $encryption_key);
        $result[0]['contractor_employees']['last_name'] = Security::decrypt(base64_decode($result[0]['contractor_employees']['last_name']), $encryption_key);
        if($result[0]['contractor_employees']['second_last_name'] != NULL ) {
            $result[0]['contractor_employees']['second_last_name'] = Security::decrypt(base64_decode($result[0]['contractor_employees']['second_last_name']), $encryption_key);
        }
     
        $this->set(compact('result'));
    }

    public function decryptEmployeeData($employee_inforrmation  = array() , $encryption_key  = ""  )  {
        $employee_inforrmation[0]['contractor_employees']['name'] = Security::decrypt(base64_decode($employee_inforrmation[0]['contractor_employees']['name']), $encryption_key);
        $employee_inforrmation[0]['contractor_employees']['last_name'] = Security::decrypt(base64_decode($employee_inforrmation[0]['contractor_employees']['last_name']), $encryption_key);
        if($employee_inforrmation[0]['contractor_employees']['second_last_name'] != NULL ) {
            $employee_inforrmation[0]['contractor_employees']['second_last_name'] = Security::decrypt(base64_decode($employee_inforrmation[0]['contractor_employees']['second_last_name']), $encryption_key);
        }

        return $employee_inforrmation;

    }

    public function edit($id = null ) {
        $session = $this->getRequest()->getSession();
        $encryption_key = $session->read('UserInfo.Key');
        $company_id = $session->read('UserInfo.CompanyID');
        $contractor_employee_document = $this->ContractorEmployeeDocuments->newEmptyEntity();
        $query = $this->ContractorEmployeeDocuments->find('all')->select(["ContractorEmployeeDocuments.id" , "ContractorEmployeeDocuments.contractor_employee_id",
        "ContractorEmployeeDocuments.contractor_document_type_id", "ContractorEmployeeDocuments.document_file", "ContractorEmployeeDocuments.extension",
        "ContractorEmployeeDocuments.comments", "ContractorEmployeeDocuments.status", "contractor_document_types.description" , "contractor_employees.name" , "contractor_employees.last_name" , 
         "contractor_employees.second_last_name" , "contractor_employees.visit_company_id"
         ])
        ->join([
            'table' => 'contractor_document_types',
            'type' => 'LEFT',
            'conditions' => 'ContractorEmployeeDocuments.contractor_document_type_id = contractor_document_types.id'
        ])
        ->join([
            'table' => 'contractor_employees',
            'type' => 'LEFT',
            'conditions' => 'ContractorEmployeeDocuments.contractor_employee_id = contractor_employees.id'
        ])
        ->where(['ContractorEmployeeDocuments.id' => $id]);
        $query->enableHydration(false); // Results as arrays instead of entities
        $result = $query->toList();
        
        if(count($result) == 0 ) {
            $this->Flash->error(__('No se encontraron datos '));
            return $this->redirect(['controller'=>'visit_companies','action' => 'view', $company_id]);
        }
        if($result[0]['contractor_employees']['visit_company_id'] != $company_id ) {
            $this->Flash->error(__('Información No dispoible. '));
            return $this->redirect(['controller'=>'visit_companies','action' => 'view', $company_id]);
        }

        
        $result[0]['contractor_employees']['name'] = Security::decrypt(base64_decode($result[0]['contractor_employees']['name']), $encryption_key);
        $result[0]['contractor_employees']['last_name'] = Security::decrypt(base64_decode($result[0]['contractor_employees']['last_name']), $encryption_key);
        if($result[0]['contractor_employees']['second_last_name'] != NULL ) {
            $result[0]['contractor_employees']['second_last_name'] = Security::decrypt(base64_decode($result[0]['contractor_employees']['second_last_name']), $encryption_key);
        }
    
        $employee_id = $result[0]['contractor_employee_id'];
        if($this->request->is('post')) {
             $contractor_employee_document->status = 'Registrado';
             $contractor_employee_document->id = $id;
             $session = $this->getRequest()->getSession();
 
             $encryption_key = $session->read('UserInfo.Key');
 
             $data = $this->request->getData();
             $target_path = (WWW_ROOT.'img_upload');
             foreach($data as $name => $value) {
                 
                 if(is_object($value)  ) {
                     if($value->getSize() > 0 ) {
                        if(!in_array($value->getclientMediaType(),$this->allowed_files) )  {
                            $this->Flash->error(__('Tipo de documento ['.$value->getclientMediaType().'] - No Válido'));
                            return $this->redirect(['action' => 'edit' , $id]);
                        }  
                         $file_path = $target_path.DS.time().$value->getclientFilename();
                         $value->moveTo($file_path); 
                         $file_base_64 = base64_encode(file_get_contents($file_path));
                         $media_type  = $value->getclientMediaType();
                         $extension_encrypted = Security::encrypt($media_type, $encryption_key);
                         $contractor_employee_document->extension = base64_encode($extension_encrypted);
                         $file_enc = Security::encrypt($file_base_64, $encryption_key);
                         $file_enc_base  = base64_encode($file_enc);
                         $contractor_employee_document->document_file = $file_enc_base;
                         $contractor_employee_document->update_date = FrozenTime::now();
                         $contractor_employee_document->comments = $data['comments'];
                         $contractor_employee_document->editable = 0 ;
                         $this->ContractorEmployeeDocuments->save($contractor_employee_document);
                         unlink($file_path);
     
                     }
                     else {
                         $this->Flash->error(__('El campo de archivo no puede ir vacio'));
                         return $this->redirect(['action' => 'edit']);
                     }
                     
                 }
               
             }
 
             $this->Flash->success(__('Archivo agregado '));
             return $this->redirect(['controller'=>'contractor_employees','action' => 'view' , $employee_id]);
         }

        $this->set('result' , $result);
        $this->set('contractor_employee_document' , $contractor_employee_document);
        $this->set('document_id' , $id);

    }
    public function validateCourses($employee_id = "" , $visit_company = '' ) {
        $connection = ConnectionManager::get('default');       

        $required_courses_query = "select a.contractor_training_id from external_type_training a 
        left join visit_companies b on a.visit_external_Type_id = b.visit_external_type_id  
        where b.id = '".$visit_company ."' and a.required = 1 ";
        $required_courses = $connection->execute($required_courses_query)->fetchAll('assoc');
        $missing_courses = array();
        if(count($required_courses)  > 0  ) {
            foreach ($required_courses as $key=> $course ) {
                $count_course_query = "select a.description , count(*) as has_trainig 
                from contractor_training a left join contractor_employee_training b on a.id = b.contractor_training_id
                where contractor_employee_id = ".$employee_id." 
                and contractor_training_id = ".$course['contractor_training_id']." and score >= 80";
                $course_check = $connection->execute($count_course_query)->fetchAll('assoc');
                if($course_check[0]['has_trainig'] == 0 ) {
                    $missing_courses[] = $course_check[0]['description'];
                }
            }
        }
        return $missing_courses;
        
    }

    public function add($employee_id = null){
        $session = $this->getRequest()->getSession();
        $company_id = $session->read('UserInfo.CompanyID');
        $missing_courses = $this->validateCourses($employee_id, $company_id );
        if(count($missing_courses) > 0 ) {
            $mensaje = "El usuario debe aprobar las evaluaciones : " . implode(" , " ,$missing_courses)." antes de subir su expediente."; 
            $this->Flash->error(__($mensaje));
            return $this->redirect(['controller'=>'visit-companies','action' => 'view' ,  $company_id]);
        }
        
        $contractor_employee_document = $this->ContractorEmployeeDocuments->newEmptyEntity();
        $document_types = TableRegistry::getTableLocator()->get('ContractorDocumentTypes');
        $query = $document_types->find()->where(['type =' => 'personal' , 'status' => 'activo']);
        
        $query->enableHydration(false); // Results as arrays instead of entities
        $result = $query->toList(); 
        foreach($result as $document => $data) {
            $documents_results[$data['id']] = $data['description'];   
        }
        // Execute the query and return the array
        $this->set(compact('documents_results', 'employee_id'));

        if($this->request->is('post')) {
           // $contractor_employee_document = $this->ContractorEmployeeDocuments->patchEntity($contractor_employee_document , $this->request->getData());
            $contractor_employee_document->status = 'Registrado';
            $session = $this->getRequest()->getSession();

            /*
            
             $last_name_enc = Security::encrypt($data['last_name'], $encryption_key);
            $last_name_enc_base64  = base64_encode($last_name_enc);
            $entity_values['last_name'] = $last_name_enc_base64;
            
            */
            $encryption_key = $session->read('UserInfo.Key');
            $data = $this->request->getData();
            $target_path = (WWW_ROOT.'img_upload');
            foreach($data as $name => $value) {
                if(is_object($value)  ) {
                    if($value->getSize() > 0 ) {
                        $size = $value->getSize(); 
                        $size_in_mb = ($size/1024) / 1024;
                        if($size_in_mb >= 2 ) {
                        $this->Flash->error(__('Tu archivo pesa [ '.number_format($size_in_mb,3).' MBs ] - El Archivo no puede pesar mas de 2MBs' ));
                        return $this->redirect(['action' => 'add' , $employee_id]);
                        }
                        if(!in_array($value->getclientMediaType(),$this->allowed_files) )  {
                            $this->Flash->error(__('Tipo de documento ['.$value->getclientMediaType().'] - No Válido'));
                            return $this->redirect(['action' => 'add' , $employee_id]);
                        }    
                        $file_path = $target_path.DS.time().$value->getclientFilename();
                        $value->moveTo($file_path); 
                        $file_base_64 = base64_encode(file_get_contents($file_path));
                        $contractor_employee_document->contractor_employee_id = $employee_id;
                        $contractor_employee_document->contractor_document_type_id = $data['document_type_id'];
                        $media_type  = $value->getclientMediaType();
                        $extension_encrypted = Security::encrypt($media_type, $encryption_key);
                        $contractor_employee_document->extension = base64_encode($extension_encrypted);
                        $file_enc = Security::encrypt($file_base_64, $encryption_key);
                        $file_enc_base  = base64_encode($file_enc);
                        $contractor_employee_document->document_file = $file_enc_base;
                        $contractor_employee_document->insert_date = FrozenTime::now();
                        $contractor_employee_document->comments = $data['comments'];
                        $this->ContractorEmployeeDocuments->save($contractor_employee_document);
                        unlink($file_path);
                    }
                    else {
                        $this->Flash->error(__('Tu archivo no tiene datos o el campo esta vacio'));
                        return $this->redirect(['action' => 'add' , $employee_id]);
                    }
                }
            }
            $this->Flash->success(__('Archivo agregado '));
            return $this->redirect(['controller'=>'contractor_employees','action' => 'view' , $employee_id]);
        }
                $this->set('contractor_employee_document', $contractor_employee_document);
    }
}