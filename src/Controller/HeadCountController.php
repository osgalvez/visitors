<?php
// src/Controller/ArticlesController.php
namespace App\Controller;
use Cake\ORM\TableRegistry;
use Cake\Datasource\ConnectionManager;
use Cake\I18n\FrozenTime;
use Cake\Utility\Security;
use Cake\Database\Connection;
use Cake\Database\Driver\Sqlserver;


class HeadCountController  extends AppController
{
    private         $connection_zmxpm ;
    public function beforeFilter(\Cake\Event\EventInterface $event)
    {   
        parent::beforeFilter($event);
        $this->Authentication->allowUnauthenticated(['pushEmployeeData']);
    }
    public function initialize(): void
    {
        ConnectionManager::setConfig('zoltekmx', [
            'className' => 'Cake\Database\Connection',
            'driver' => 'Cake\Database\Driver\Mysql',
            'persistent' => false,
            'host' => '209.50.52.248',
            'username' => 'zoltek',
            'password' => 'Yhzt3_53',
            'database' => 'zoltek_visitors',
            'encoding' => 'utf8',
            'timezone' => 'UTC',
            'cacheMetadata' => true,
        ]);
            
        parent::initialize();
        $this->loadComponent('Flash'); // Include the FlashComponent
        $this->connection_zmxpm = ConnectionManager::get('zoltekmx');

        
    }
    public function index()
    {   
        $session = $this->getRequest()->getSession(); 
        $person_id =  $session->read('UserInfo.CompanyID');
        if($person_id != 'admin') {
            $this->Flash->error(__('No tienes acceso a la pagina'));        
            return $this->redirect(['controller' => 'visit-companies' , 'action' => 'index']);
        }

        $count_visitor = "SELECT COUNT(distinct(visitor)) as TotalVisitors FROM visits where status = 'active' and visitor_type = 1";
        $visitor_detail = "SELECT visitor, GROUP_CONCAT(visited_area ORDER BY visited_area SEPARATOR '{{+}}') AS VisitedArea FROM visits where status = 'active' and visitor_type = 1 GROUP BY visitor ORDER BY visitor;";
        $visitor_count = $this->connection_zmxpm->execute($visitor_detail)->fetchAll('assoc');
        $total_visitors = $this->connection_zmxpm->execute($count_visitor)->fetchAll('assoc');
        /**/
        $count_suppliers_query = "SELECT COUNT(distinct(visitor)) as TotalSuppliers FROM visits where status = 'active' and visitor_type = 6";
        $suppliers_query = "SELECT visitor, GROUP_CONCAT(visited_area ORDER BY visited_area SEPARATOR '{{+}}') AS VisitedArea FROM visits where status = 'active' and visitor_type = 6 GROUP BY visitor ORDER BY visitor;";
        $supplier_count = $this->connection_zmxpm->execute($count_suppliers_query)->fetchAll('assoc');
        $supplier_detail = $this->connection_zmxpm->execute($suppliers_query)->fetchAll('assoc');
        $total_suppliers = $supplier_count[0]['TotalSuppliers'];
        $suppliers_decrypted = array();

        /**/ 
        $empoloyees_query = "SELECT CASE WHEN Area = '' THEN 'Otra' ELSE Area end  as Area, COUNT(UserID) Employees from employees_in_plant eip where status= 'En Planta'
        GROUP BY eip.Area ORDER BY 1 asc";
        $total_employees_query = "SELECT COUNT(*) AS Total FROM employees_in_plant WHERE status= 'En Planta'";
        $employees_by_area = $this->connection_zmxpm->execute($empoloyees_query)->fetchAll('assoc');
        $total_employees = $this->connection_zmxpm->execute($total_employees_query)->fetchAll('assoc');
        $total_visitors = $total_visitors[0]['TotalVisitors'];
        $visitors_decrypted = array();

        $contractors_a = $this->getContractorsInPlant("2");
        $contractors_a_count = $this->getCountContractorsInPlant("2");
        $contractors_b = $this->getContractorsInPlant("3");
        $contractors_b_count = $this->getCountContractorsInPlant("3");
        $contractors_c = $this->getContractorsInPlant("4");
        $contractors_c_count = $this->getCountContractorsInPlant("4");
        $carriers = $this->getContractorsInPlant("5");
        $carriers_count = $this->getCountContractorsInPlant("5");

        if($total_visitors >  0 )  {
            $ciphering = "AES-128-CTR"; 
            $iv_length = openssl_cipher_iv_length($ciphering); 
            $options = 0;
            
            
            // Non-NULL Initialization Vector for encryption 
            $encryption_iv = '1234567891011121'; 
            // Store the encryption key 
            $encryption_key = "ZTKMX-0022"; 
            // Use openssl_encrypt() function to encrypt the data 
            
            $visitor_decrypted = openssl_decrypt($visitor_count[0]['visitor'], $ciphering, 
            $encryption_key, $options, $encryption_iv); 
            
    
            foreach($visitor_count as $key => $values) {
                $visited_areas = array();
                $visitor_decrypted = openssl_decrypt($values['visitor'], $ciphering, $encryption_key, $options, $encryption_iv); 
                $visitors_decrypted[$key]['visitor'] = $visitor_decrypted ;
                $visited_area_exploded = explode("{{+}}" , $values['VisitedArea'] );
                foreach($visited_area_exploded as $index => $value ) {
                    $visited_area_decrypted = openssl_decrypt($value, $ciphering, $encryption_key, $options, $encryption_iv); 
                    $visited_areas[] = $visited_area_decrypted;
                }
                $visitors_decrypted[$key]['VisitedAreas'] = implode(" , " , $visited_areas);
    
            }
        }

        if($total_suppliers >  0 )  {
            $ciphering = "AES-128-CTR"; 
            $iv_length = openssl_cipher_iv_length($ciphering); 
            $options = 0;
            
            
            // Non-NULL Initialization Vector for encryption 
            $encryption_iv = '1234567891011121'; 
            // Store the encryption key 
            $encryption_key = "ZTKMX-0022"; 
            // Use openssl_encrypt() function to encrypt the data 
            
            $visitor_decrypted = openssl_decrypt($visitor_count[0]['visitor'], $ciphering, 
            $encryption_key, $options, $encryption_iv); 
            
    
            foreach($supplier_detail as $key => $values) {
                $visited_areas_supplier = array();
                $supplier_decrypted = openssl_decrypt($values['visitor'], $ciphering, $encryption_key, $options, $encryption_iv); 
                $suppliers_decrypted[$key]['visitor'] = $supplier_decrypted ;
                $visited_area_supplier = explode("{{+}}" , $values['VisitedArea'] );
                foreach($visited_area_supplier as $index => $value ) {
                    $visited_area_decrypted = openssl_decrypt($value, $ciphering, $encryption_key, $options, $encryption_iv); 
                    $visited_areas_supplier[] = $visited_area_decrypted;
                }
                $suppliers_decrypted[$key]['VisitedAreas'] = implode(" , " , $visited_areas_supplier);
    
            }
        }
        
       
    $this->set('count_visitor' , $total_visitors);
    $this->set('visitors_decrypted',$visitors_decrypted);
    $this->set('employees_by_area' , $employees_by_area);
    $this->set('total_employees',$total_employees[0]['Total']);
    $this->set('contractors_a_count',$contractors_a_count);
    $this->set('contractors_a',$contractors_a);
    $this->set('contractors_b_count',$contractors_b_count);
    $this->set('contractors_b',$contractors_b);
    $this->set('contractors_c_count',$contractors_c_count);
    $this->set('contractors_c',$contractors_c);
    $this->set('carriers',$carriers);
    $this->set('carriers_count',$carriers_count);
    $this->set('total_suppliers',$total_suppliers);
    $this->set('suppliers_decrypted',$suppliers_decrypted);


    }

    public function getContractorsInPlant($contractor_type) {
        $contractor_query = "select count(attendance.contractor_id) as number, companies.id , companies.name , users.password
        from contractor_attendance attendance 
        left join contractor_employees employees
        on attendance.contractor_id = employees.id
        left join visit_companies companies on employees.visit_company_id = companies.id
        left join users on users.username = companies.id
        left join visit_external_types external_types on companies.visit_external_type_id = external_types.id
        where attendance.active = 1 and companies.visit_external_type_id = $contractor_type group by companies.id order by 1 desc ";
        $contractor_array = $this->connection_zmxpm->execute($contractor_query)->fetchAll('assoc');
        if(count($contractor_array) > 0 ) {
            foreach($contractor_array as $index => $data_detail ) {
                $contractor_array[$index]['name'] =   Security::decrypt(base64_decode($data_detail['name']), $data_detail['password'])  ;
                unset($contractor_array[$index]['password']);
            }
        }
        return $contractor_array;  
    }

    public function getCountContractorsInPlant($contractor_type) {
        $contractor_query = "select count(attendance.id) as countContractor 
        from contractor_attendance attendance 
        left join contractor_employees employees
        on attendance.contractor_id = employees.id
        left join visit_companies companies on employees.visit_company_id = companies.id
        where attendance.active = 1 and companies.visit_external_type_id =  $contractor_type  ";
        $contractorCount = $this->connection_zmxpm->execute($contractor_query)->fetchAll('assoc');
        $contractor_count = $contractorCount[0]['countContractor'];
        return $contractor_count;
    }

}