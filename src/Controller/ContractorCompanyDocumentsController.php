<?php
// src/Controller/ArticlesController.php
namespace App\Controller;
use Cake\ORM\TableRegistry;
use Cake\I18n\FrozenTime;
use Authentication\PasswordHasher\DefaultPasswordHasher;
use Cake\Utility\Hash;
use Cake\Utility\Security;
use Cake\Datasource\ConnectionManager;


class ContractorCompanyDocumentsController extends AppController
{   
    public $allowed_files;
    public function initialize(): void
    {
        $this->allowed_files = array('image/jpeg', 'image/png' , 'application/pdf');
        ConnectionManager::setConfig('zoltekmx', [
            'className' => 'Cake\Database\Connection',
            'driver' => 'Cake\Database\Driver\Mysql',
            'persistent' => false,
            'host' => '209.50.52.248',
            'username' => 'zoltek',
            'password' => 'Yhzt3_53',
            'database' => 'zoltek_visitors',
            'encoding' => 'utf8',
            'timezone' => 'UTC',
            'cacheMetadata' => true,
        ]);
        parent::initialize();

        $this->loadComponent('Paginator');
        $this->loadComponent('Flash'); // Include the FlashComponent
    }
    public function index()
    {
        $this->loadComponent('Paginator');
        $contractor_document = $this->Paginator->paginate($this->ContractorCompanyDocuments->find());
        $this->set(compact('contractor_document'));
    }

    public function view($id = null ) {
         
        $session = $this->getRequest()->getSession();
        $encryption_key = $session->read('UserInfo.Key');
        $company_id = $session->read('UserInfo.CompanyID');


        $query = $this->ContractorCompanyDocuments->find('all')->select(["ContractorCompanyDocuments.id" , "ContractorCompanyDocuments.visit_company_id",
        "ContractorCompanyDocuments.contractor_document_type_id", "ContractorCompanyDocuments.document_file", "ContractorCompanyDocuments.extension",
         "ContractorCompanyDocuments.status", "contractor_document_types.description" , "visit_companies.id" , "visit_companies.name" , "ContractorCompanyDocuments.editable"
         ])
        ->join([
            'table' => 'contractor_document_types',
            'type' => 'LEFT',
            'conditions' => 'ContractorCompanyDocuments.contractor_document_type_id = contractor_document_types.id'
        ])
        ->join([
            'table' => 'visit_companies',
            'type' => 'LEFT',
            'conditions' => 'ContractorCompanyDocuments.visit_company_id = visit_companies.id'
        ])
        ->where(['ContractorCompanyDocuments.id' => $id]);
        $query->enableHydration(false); // Results as arrays instead of entities
        $result = $query->toList();
        if(count( $result) == 0 ) {
            $this->Flash->error(__('No existen datos.'));
            return $this->redirect(['controller'=>'visit_companies','action' => 'index']);
        }

        if($result[0]['visit_company_id'] != $company_id) {
            $this->Flash->error(__('Acceso Denegado'));
            return $this->redirect(['controller'=>'visit_companies','action' => 'index']);
        }

        
        if($result[0]['document_file'] == NULL || $result[0]['extension']  == NULL ) {
            
            $this->Flash->error(__('Archivo no disponible.'));
            return $this->redirect(['controller'=>'visit_companies','action' => 'index']);

        }

        $result[0]['document_file'] = Security::decrypt(base64_decode($result[0]['document_file']), $encryption_key);
        $result[0]['extension'] = Security::decrypt(base64_decode($result[0]['extension']), $encryption_key);
        
        $result[0]['visit_companies']['name'] = Security::decrypt(base64_decode($result[0]['visit_companies']['name']), $encryption_key);
        $result[0]['visit_companies']['id'] = Security::decrypt(base64_decode($result[0]['visit_companies']['id']), $encryption_key);
       
     
        $this->set(compact('result'));
    }

    public function add($company_id = null){
        $session = $this->getRequest()->getSession();
        $logged_company = $session->read('UserInfo.CompanyID');
        if($logged_company != $company_id) {
            $this->Flash->error(__('Acceso Denegado.'));
            return $this->redirect(['controller'=>'visit_companies','action' => 'index']);
        }
        $contractor_employee_document = $this->ContractorCompanyDocuments->newEmptyEntity();
        $document_types = TableRegistry::getTableLocator()->get('ContractorDocumentTypes');
        $query = $document_types->find()->where(['type =' => 'empresa' , 'status' => 'activo']);
        
        $query->enableHydration(false); // Results as arrays instead of entities
        $result = $query->toList(); 
        foreach($result as $document => $data) {
            $documents_results[$data['id']] = $data['description'];   
        }
        // Execute the query and return the array
        $this->set(compact('documents_results', 'company_id'));
            
        if($this->request->is('post')) {
            
            $contractor_employee_document->status = 'Registrado';

            
            $encryption_key = $session->read('UserInfo.Key');
            $data = $this->request->getData();
            $target_path = (WWW_ROOT.'img_upload');
           
            foreach($data as $name => $value) {
                if(is_object($value)  ) {
                    if($value->getSize() > 0 ) {
                        $size = $value->getSize(); 
                        $size_in_mb = ($size/1024) / 1024;
                        if($size_in_mb >= 2 ) {
                        $this->Flash->error(__('Tu archivo pesa [ '.number_format($size_in_mb,3).' MBs ] - El Archivo no puede pesar mas de 2MBs' ));
                        return $this->redirect(['action' => 'add' , $company_id]);
                        }
                        
                        if(!in_array($value->getclientMediaType(),$this->allowed_files) )  {
                            $this->Flash->error(__('Tipo de documento ['.$value->getclientMediaType().'] - No Válido'));
                            return $this->redirect(['action' => 'add' , $company_id]);
                        }                        
                        $file_path = $target_path.DS.time().$value->getclientFilename();
                        $value->moveTo($file_path); 
                        $file_base_64 = base64_encode(file_get_contents($file_path));
                        $contractor_employee_document->visit_company_id = $company_id;
                        $contractor_employee_document->contractor_document_type_id = $data['document_type_id'];

                        $media_type  = $value->getclientMediaType();
                        $extension_encrypted = Security::encrypt($media_type, $encryption_key);
                        $contractor_employee_document->extension = base64_encode($extension_encrypted);

                        $file_enc = Security::encrypt($file_base_64, $encryption_key);
                        $file_enc_base  = base64_encode($file_enc);
                        $contractor_employee_document->document_file = $file_enc_base;
                        $contractor_employee_document->insert_date = FrozenTime::now();
                        $contractor_employee_document->comments = $data['comments'];

                        $this->ContractorCompanyDocuments->save($contractor_employee_document);
                        unlink($file_path);
    
                    }
                    else {
                        $this->Flash->error(__('Tu archivo no tiene datos o el campo esta vacio'));
                        return $this->redirect(['action' => 'add' , $company_id]);
                    }
                    
                }
              
            }

            $this->Flash->success(__('Archivo agregado '));
            return $this->redirect(['controller'=>'visit_companies','action' => 'index']);

         
        }

                $this->set('contractor_employee_document', $contractor_employee_document);
             

    }


    public function edit($id = null ) {
        $session = $this->getRequest()->getSession();
        $encryption_key = $session->read('UserInfo.Key');
        $company_id = $session->read('UserInfo.CompanyID');
        $contractor_conpany_document = $this->ContractorCompanyDocuments->newEmptyEntity();
        $query = $this->ContractorCompanyDocuments->find('all')->select(["ContractorCompanyDocuments.id" , "ContractorCompanyDocuments.visit_company_id",
        "ContractorCompanyDocuments.contractor_document_type_id", "ContractorCompanyDocuments.document_file", "ContractorCompanyDocuments.extension",
        "ContractorCompanyDocuments.comments", "ContractorCompanyDocuments.status", "contractor_document_types.description","visit_companies.name" 
         ])
        ->join([
            'table' => 'contractor_document_types',
            'type' => 'LEFT',
            'conditions' => 'ContractorCompanyDocuments.contractor_document_type_id = contractor_document_types.id'
        ])
        ->join([
            'table' => 'visit_companies',
            'type' => 'LEFT',
            'conditions' => 'ContractorCompanyDocuments.visit_company_id = visit_companies.id'
        ])
        ->where(['ContractorCompanyDocuments.id' => $id]);
        $query->enableHydration(false); // Results as arrays instead of entities
        $result = $query->toList();
        
        if(count($result) == 0 ) {
            $this->Flash->error(__('No se encontraron datos '));
            return $this->redirect(['controller'=>'visit_companies','action' => 'index']);
        }
        if($result[0]['visit_company_id'] != $company_id ) {
            $this->Flash->error(__('Información No dispoible. '));
            return $this->redirect(['controller'=>'visit_companies','action' => 'index']);
        }
        $result[0]['visit_companies']['name'] = Security::decrypt(base64_decode($result[0]['visit_companies']['name']), $encryption_key);

        
    
        if($this->request->is('post')) {
             $contractor_conpany_document->status = 'Registrado';
             $contractor_conpany_document->id = $id;
             $session = $this->getRequest()->getSession();
 
             $encryption_key = $session->read('UserInfo.Key');
 
             $data = $this->request->getData();
             $target_path = (WWW_ROOT.'img_upload');
             foreach($data as $name => $value) {
                 
                 if(is_object($value)  ) {
                     if($value->getSize() > 0 ) {
                        if(!in_array($value->getclientMediaType(),$this->allowed_files) )  {
                            $this->Flash->error(__('Tipo de documento ['.$value->getclientMediaType().'] - No Válido'));
                            return $this->redirect(['action' => 'edit' , $id]);
                        }  
                         $file_path = $target_path.DS.time().$value->getclientFilename();
                         $value->moveTo($file_path); 
                         $file_base_64 = base64_encode(file_get_contents($file_path));
                         $media_type  = $value->getclientMediaType();
                         $extension_encrypted = Security::encrypt($media_type, $encryption_key);
                         $contractor_conpany_document->extension = base64_encode($extension_encrypted);
                         $file_enc = Security::encrypt($file_base_64, $encryption_key);
                         $file_enc_base  = base64_encode($file_enc);
                         $contractor_conpany_document->document_file = $file_enc_base;
                         $contractor_conpany_document->update_date = FrozenTime::now();
                         $contractor_conpany_document->comments = $data['comments'];
                         $contractor_conpany_document->editable = 0 ;
                         $this->ContractorCompanyDocuments->save($contractor_conpany_document);
                         unlink($file_path);
     
                     }
                     else {
                         $this->Flash->error(__('El campo de archivo no puede ir vacio'));
                         return $this->redirect(['action' => 'edit']);
                     }
                     
                 }
               
             }
 
             $this->Flash->success(__('Archivo Editado '));
             return $this->redirect(['controller'=>'visit_companies','action' => 'index']);
            }

        $this->set('result' , $result);
        $this->set('contractor_conpany_document' , $contractor_conpany_document);
        $this->set('document_id' , $id);

    }

    
}