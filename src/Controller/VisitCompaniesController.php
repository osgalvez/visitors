<?php
// src/Controller/ArticlesController.php
namespace App\Controller;
use Cake\ORM\TableRegistry;
use  Cake\Utility\Security;
use Cake\Core\Configure;
use Cake\Core\Configure\Engine\PhpConfig;
class VisitCompaniesController extends AppController
{   

    public $encrypted_columns ; 
    public function initialize(): void
    {   
        $this->encrypted_columns = array('name' , 'rfc' , 'last_name' , 'second_last_name' , 'ssn');
        parent::initialize();
        $this->loadComponent('Paginator');
        $this->loadComponent('Flash'); // Include the FlashComponent
    }
    public function index()
    {   
        $session = $this->getRequest()->getSession();
        $company_id = $session->read('UserInfo.CompanyID');
        $key_decript = $session->read('UserInfo.Key');
        $companies = $this->VisitCompanies->find('all')->where(['id' => $company_id])->toList();

        foreach($companies as $key =>  $company ) {
            $companies[$key]['rfc']  =  Security::decrypt(base64_decode($companies[$key]['rfc']), $key_decript);
            $companies[$key]['name']  =  Security::decrypt(base64_decode($companies[$key]['name']), $key_decript);

        }

        $company_documents_table = TableRegistry::getTableLocator()->get('ContractorCompanyDocuments');
        $query = $company_documents_table->find('all')->select(["ContractorCompanyDocuments.id" , 
         "ContractorCompanyDocuments.status", "contractor_document_types.description" ,  "ContractorCompanyDocuments.editable"
         ])
        ->join([
            'table' => 'contractor_document_types',
            'type' => 'LEFT',
            'conditions' => 'ContractorCompanyDocuments.contractor_document_type_id = contractor_document_types.id'
        ])
        ->join([
            'table' => 'visit_companies',
            'type' => 'LEFT',
            'conditions' => 'ContractorCompanyDocuments.visit_company_id = visit_companies.id'
        ])
        ->where(['ContractorCompanyDocuments.visit_company_id' => $company_id]);
        $query->enableHydration(false); // Results as arrays instead of entities
        $document_list = $query->toList(); 


        /*
        $query = $this->ContractorCompanyDocuments->find('all')->select(["ContractorCompanyDocuments.id" , "ContractorCompanyDocuments.visit_company_id",
        "ContractorCompanyDocuments.contractor_document_type_id", "ContractorCompanyDocuments.document_file", "ContractorCompanyDocuments.extension",
         "ContractorCompanyDocuments.status", "contractor_document_types.description" , "visit_companies.id" , "visit_companies.name" , "ContractorCompanyDocuments.editable"
         ])
        ->join([
            'table' => 'contractor_document_types',
            'type' => 'LEFT',
            'conditions' => 'ContractorCompanyDocuments.contractor_document_type_id = contractor_document_types.id'
        ])
        ->join([
            'table' => 'visit_companies',
            'type' => 'LEFT',
            'conditions' => 'ContractorCompanyDocuments.visit_company_id = visit_companies.id'
        ])
        ->where(['ContractorCompanyDocuments.id' => $id]);
        
        */
        $this->set(compact('companies'));

        $this->set(compact('document_list'));
    }
    public function view($id = '' ) {
        $session = $this->getRequest()->getSession();
        $company_id = $session->read('UserInfo.CompanyID');
        $encryption_key = $session->read('UserInfo.Key');
        if($company_id != $id ) {
        $this->Flash->error(__('Solo puedes ver informacion de tu compañia') ) ;
        return $this->redirect(['controller'=>'visit_companies','action' => 'index' ]);
        }
        $employees_table = TableRegistry::getTableLocator()->get('ContractorEmployees');
        $query = $employees_table->find()
        ->where(['ContractorEmployees.visit_company_id' => $id]);
        $query->enableHydration(false); // Results as arrays instead of entities
        $employees_list = $query->toList(); 
        if(count($employees_list) > 0 ) {
            foreach($employees_list as $index => $values  ) {
                foreach($values  as $named_index => $data_value ) {
                    if(in_array($named_index,$this->encrypted_columns) &&  $data_value !=  '' ) {
                        
                        $value_base64_decoded = base64_decode($data_value);
                        $value_dencrypted =  Security::decrypt($value_base64_decoded, $encryption_key);
                        $employees_list[$index][$named_index] = $value_dencrypted;
                    }
                }
            }

        } 
        $company = $this->VisitCompanies->findById($id)
                ->enableHydration(false)
                ->toList();
        foreach($company as $index => $values  ) {
            foreach($values  as $named_index => $data_value ) {
                if(in_array($named_index,$this->encrypted_columns) &&  $data_value !=  '' ) {
                    $value_base64_decoded = base64_decode($data_value);
                    $value_dencrypted =  Security::decrypt($value_base64_decoded, $encryption_key);
                    $company[$index][$named_index] = $value_dencrypted;
                }
            }
        }
        $this->set(compact('company', 'employees_list'));
    }


}