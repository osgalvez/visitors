<?php
// src/Controller/ArticlesController.php
namespace App\Controller;
use Cake\I18n\FrozenTime;
use Cake\ORM\TableRegistry;
class VisitIdDocumentsController extends AppController
{
    public function initialize(): void
    {
        parent::initialize();

        $this->loadComponent('Paginator');
        $this->loadComponent('Flash'); // Include the FlashComponent
    }
    public function index()
    {
        $this->loadComponent('Paginator');
        $visits = $this->Paginator->paginate($this->VisitVisitorVisits->find());
        $this->set(compact('visits'));
    }

    public function view($id = null ) {
        $visitor = $this->VisitVisitorVisits->findById($id)->firstOrFail();
        $this->set(compact('visitor'));
    }

    public function visitExit($id= null ){
        $visit = $this->VisitVisitorVisits->findById($id)->firstOrFail();
        $visit->status = 'inactive';
        $visit->exit_date =  $now = FrozenTime::now();
        

        if ($this->VisitVisitorVisits->save($visit)) {
            $this->Flash->success(__('Your visit  has been updated.'));
            return $this->redirect(['action' => 'index']);
        }
        $this->Flash->error(__('Unable to update your article.'));
        $this->set('visit', $visit);

    }

    public function add($id = false ){
     $idDocument = $this->VisitIdDocuments->newEmptyEntity();
     if($this->request->is('post')) {
          
            $data = $this->request->getData();
           
            $target_path = (WWW_ROOT.'img_upload');
            $visitor_id = $data['visit_visitor_id'];
            foreach($data as $name => $value) {
                
                if(is_object($value)  ) {
                
                    if($value->getSize() > 0 ) {
                        $file_path = $target_path.DS.time().$value->getclientFilename();
                        $value->moveTo($file_path); 
                        $img_base64 = base64_encode(file_get_contents($file_path));
                        $idDocument->visit_visitor_id = $visitor_id;
                        $idDocument->image = $img_base64;
                        $idDocument->file_type = $value->getclientMediaType();
                        $this->VisitIdDocuments->save($idDocument);
                        unlink($file_path);
    
                    }
                    else {
                        $this->Flash->error(__('El campo de archivo no puede ir vacio'));
                        return $this->redirect(['action' => 'add' , $visitor_id]);
                    }
                    
                }
              
            }
            $this->Flash->success(__('Identificacion agregada correctamente.'));
            return $this->redirect(['controller' => 'VisitVisitors','action' => 'index']);
        }
        $this->set('idDocument', $idDocument);
        $this->set('id_visitor', $id);

    }
}