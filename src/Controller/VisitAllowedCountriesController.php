<?php
// src/Controller/ArticlesController.php
namespace App\Controller;
use Cake\ORM\TableRegistry;
use Cake\I18n\FrozenTime;

class VisitAllowedCountriesController extends AppController
{
    public function initialize(): void
    {
        $session = $this->getRequest()->getSession();
        parent::initialize();

        $this->loadComponent('Paginator');
        $this->loadComponent('Flash'); // Include the FlashComponent
    }
    public function index()
    {
        $this->loadComponent('Paginator');
        $allowed_country = $this->VisitAllowedCountries->find('all');
        $this->set(compact('allowed_country'));
    }

    public function view($id = null ) {
        $country = $this->VisitAllowedCountries->findById($id)->firstOrFail();
        $this->set(compact('country'));
    }

    public function enable($id = null ) {
        $scheduled->insert_user_id =  $session->read('UserInfo.UserID');           
            $scheduled->area_code = $session->read('UserInfo.AreaCode');
            $scheduled->area = $session->read('UserInfo.Area');
            $scheduled->visited_person_name =  $session->read('UserInfo.CommonName');

        $visit_allowed_country = $this->VisitAllowedCountries->newEmptyEntity();
        $visit_allowed_country->id = $id;
        $visit_allowed_country->status = 'Activo';
        $visit_allowed_country->updated_date = FrozenTime::now();
        $visit_allowed_country->update_user_id = FrozenTime::now();

        if( $this->VisitAllowedCountries->save($visit_allowed_country)) {
            $this->Flash->success(__('Se edito correctamente el registro. '));
          return $this->redirect(['controller'=>'visit_allowed_countries','action' => 'index']);
        }
       $this->Flash->error(__('No se pudo editar  el pais.'));

        
    }

    public function disable($id = null ) {
        $visit_allowed_country = $this->VisitAllowedCountries->newEmptyEntity();
        $visit_allowed_country->id = $id;
        $visit_allowed_country->status = 'Inactivo';
        if( $this->VisitAllowedCountries->save($visit_allowed_country)) {
            $this->Flash->success(__('Se edito correctamente el registro. '));
          return $this->redirect(['controller'=>'visit_allowed_countries','action' => 'index']);
        }
       $this->Flash->error(__('No se pudo editar  el pais.'));
    }

    public function add(){
        $visit_allowed_country = $this->VisitAllowedCountries->newEmptyEntity();
       

        // Execute the query and return the array

        if($this->request->is('post')) {
            $visit_allowed_country = $this->VisitAllowedCountries->patchEntity($visit_allowed_country , $this->request->getData());
            $visit_allowed_country->status = 'Activo';
            $session = $this->getRequest()->getSession(); 
            $visit_allowed_country->insert_user_id = $session->read('UserInfo.UserID');
            $visit_allowed_country->insert_date = FrozenTime::now();
            if( $this->VisitAllowedCountries->save($visit_allowed_country)) {
                $this->Flash->success(__('Pais Agregado a la Lista. '));
              //  $inserted_id =  $contractor_employee->id ;
              return $this->redirect(['controller'=>'visit_allowed_countries','action' => 'index']);
            }
           $this->Flash->error(__('No se pudo agregar el pais.'));
        }
          $this->set('visit_allowed_country', $visit_allowed_country);

    }
}