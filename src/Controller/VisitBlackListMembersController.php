<?php
// src/Controller/ArticlesController.php
namespace App\Controller;
use Cake\ORM\TableRegistry;
use Cake\I18n\FrozenTime;

class VisitBlackListMembersController extends AppController
{
    public function initialize(): void
    {
        parent::initialize();

        $this->loadComponent('Paginator');
        $this->loadComponent('Flash'); // Include the FlashComponent
    }
    public function index()
    {
        $this->loadComponent('Paginator');
        $black_list_member = $this->Paginator->paginate($this->VisitBlackListMembers->find());
        $this->set(compact('black_list_member'));
    }

    public function view($id = null ) {
        $member = $this->VisitBlackListMembers->findById($id)->firstOrFail();
        $this->set(compact('member'));
    }

    public function enable($id = null ) {
        $black_list_member = $this->VisitBlackListMembers->newEmptyEntity();
        $session = $this->getRequest()->getSession(); 
        $black_list_member->id = $id;
        $black_list_member->update_user_id = $session->read('UserInfo.UserID');
        $black_list_member->update_date = FrozenTime::now();
        $black_list_member->status = 'Activo';
        if( $this->VisitBlackListMembers->save($black_list_member)) {
            $this->Flash->success(__('Miembro activado '));
          //  $inserted_id =  $contractor_employee->id ;
          return $this->redirect(['controller'=>'visit_black_list_members','action' => 'index']);
        }
       $this->Flash->error(__('No se pudo agregar.'));
    
    }
    public function disable($id = null ) {
        $black_list_member = $this->VisitBlackListMembers->newEmptyEntity();
        $session = $this->getRequest()->getSession(); 
        $black_list_member->id = $id;
        $black_list_member->update_user_id = $session->read('UserInfo.UserID');
        $black_list_member->update_date = FrozenTime::now();
        $black_list_member->status = 'Inactivo';
        if( $this->VisitBlackListMembers->save($black_list_member)) {
            $this->Flash->success(__('Miembro desactivado '));
          //  $inserted_id =  $contractor_employee->id ;
          return $this->redirect(['controller'=>'visit_black_list_members','action' => 'index']);
        }
       $this->Flash->error(__('No se pudo agregar.'));
    }

    public function add(){
        $black_list_member = $this->VisitBlackListMembers->newEmptyEntity();
        $countries = TableRegistry::getTableLocator()->get('VisitAllowedCountries');
        $query = $countries->find();
        $result = $query->toList(); 
        foreach($result as $country => $data) {
            $country_result[$data['id']] = $data['country_name'];   
        }
        // Execute the query and return the array
        $this->set(compact('country_result'));

        

        // Execute the query and return the array

        if($this->request->is('post')) {
            $black_list_member = $this->VisitBlackListMembers->patchEntity($black_list_member , $this->request->getData());
            $black_list_member->status = 'Activo';
            $session = $this->getRequest()->getSession(); 
            $black_list_member->insert_user_id = $session->read('UserInfo.UserID');
            $black_list_member->insert_date = FrozenTime::now();
            if( $this->VisitBlackListMembers->save($black_list_member)) {
                $this->Flash->success(__('Miembro agregado a  la Lista. '));
              //  $inserted_id =  $contractor_employee->id ;
              return $this->redirect(['controller'=>'visit_black_list_members','action' => 'index']);
            }
           $this->Flash->error(__('No se pudo agregar.'));
        }
          $this->set('black_list_member', $black_list_member);

    }
}