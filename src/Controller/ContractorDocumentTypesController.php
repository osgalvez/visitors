<?php
// src/Controller/ArticlesController.php
namespace App\Controller;
use Cake\ORM\TableRegistry;

class ContractorDocumentTypesController extends AppController
{
    public function initialize(): void
    {
        parent::initialize();

        $this->loadComponent('Paginator');
        $this->loadComponent('Flash'); // Include the FlashComponent
    }
    public function index()
    {
        $this->loadComponent('Paginator');
        $documentTypes = $this->Paginator->paginate($this->ContractorDocumentTypes->find());
        $this->set(compact('documentTypes'));
    }

    public function view($id = null ) {
        $visitors = TableRegistry::getTableLocator()->get('VisitIdDocuments');
        $query = $visitors->find()
        ->where(['VisitIdDocuments.visit_visitor_id' => $id]);
        $query->enableHydration(false); // Results as arrays instead of entities
        $result = $query->toList(); 
        $visitor = $this->VisitVisitors->findById($id)->firstOrFail();
        $this->set(compact('visitor','result'));
    }

    public function add(){
        $contractor_document_type = $this->ContractorDocumentTypes->newEmptyEntity();
      
        if($this->request->is('post')) {
            $contractor_document_type = $this->ContractorDocumentTypes->patchEntity($contractor_document_type , $this->request->getData());
            $contractor_document_type->status = 'Activo';
            if($this->ContractorDocumentTypes->save($contractor_document_type)) {
                $this->Flash->success(__('Tipo de Documento Agregado'));
                return $this->redirect(['controller'=>'contractor_document_types','action' => 'index']);
            }
            $this->Flash->error(__('No se pudo agregar el registro'));
        }

                $this->set('contractor_document_type', $contractor_document_type);

    }
}