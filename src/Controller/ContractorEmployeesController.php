<?php
// src/Controller/ArticlesController.php
namespace App\Controller;
use Cake\ORM\TableRegistry;
use Cake\I18n\FrozenTime;
use Authentication\PasswordHasher\DefaultPasswordHasher;
use Cake\Utility\Hash;
use Cake\Utility\Security;
use Cake\Datasource\ConnectionManager;

class ContractorEmployeesController extends AppController

{   
    public $encrypted_columns;
    public $session;
    public $company_id;
    public $encryption_key;
    public function initialize(): void
    {
        parent::initialize();
        $this->loadComponent('Paginator');
        $this->loadComponent('Flash'); // Include the FlashComponent
        $this->encrypted_columns = array('extension' , 'name' , 'document_file', 'last_name' , 'second_last_name', 'ssn');

        $this->session = $this->getRequest()->getSession();
        $this->company_id = $this->session->read('UserInfo.CompanyID');
        $this->encryption_key = $this->session->read('UserInfo.Key');
        
    }
    public function index()
    {   
        $session = $this->getRequest()->getSession();
        $company = $session->read('UserInfo.CompanyID');
        $visitors = $this->ContractorEmployees->find('all')->where(["visit_company_id" => $company]);
        $this->set(compact('visitors'));
    }

    public function view($id = null ) {
        $session = $this->getRequest()->getSession();
        $company_id = $session->read('UserInfo.CompanyID');
        $encryption_key = $session->read('UserInfo.Key');
        $employee_data = $this->ContractorEmployees->findById($id)->firstOrFail();
        $employee = $employee_data->toArray();
        
        if(count($employee) == 0) {
            $this->Flash->error(__('No se encontraron Datos'));
            return $this->redirect(['controller'=>'visit_companies','action' => 'index']);
        }
        if($employee['visit_company_id'] != $company_id){
         $this->Flash->error(__('Solo puedes ver informacion de tu compañia'));
         return $this->redirect(['controller'=>'visit_companies','action' => 'index']);
        } 

        $employee_documents = TableRegistry::getTableLocator()->get('ContractorEmployeeDocuments');
        $query = $employee_documents->find('all')->select(["ContractorEmployeeDocuments.id" , 
        "ContractorEmployeeDocuments.contractor_document_type_id", "ContractorEmployeeDocuments.name", "ContractorEmployeeDocuments.extension",
         "ContractorEmployeeDocuments.status", "ContractorEmployeeDocuments.editable" , "contractor_document_types.description"])
        ->join([
            'table' => 'contractor_document_types',
            'type' => 'LEFT',
            'conditions' => 'ContractorEmployeeDocuments.contractor_document_type_id = contractor_document_types.id']) 
        ->where(['ContractorEmployeeDocuments.contractor_employee_id' => $id]);
        $query->enableHydration(false); // Results as arrays instead of entities
        $result = $query->toList();
        if(count($result) > 0 ) {
            foreach($result as $index => $values  ) {
                foreach($values  as $named_index => $data_value ) {
                    if(in_array($named_index,$this->encrypted_columns) &&  $data_value !=  '' ) {
                        
                        $value_base64_decoded = base64_decode($data_value);
                        $value_dencrypted =  Security::decrypt($value_base64_decoded, $encryption_key);
                        $result[$index][$named_index] = $value_dencrypted;
                    }
                }
            }
        } 

        else {
            $this->Flash->error(__('No se encontraron Datos'));
            return $this->redirect(['controller'=>'visit_companies','action' => 'index']);
        }
        
        foreach($employee  as $named_index => $data_value ) {
            if(in_array($named_index,$this->encrypted_columns) &&  $data_value !=  '' ) {
                $value_base64_decoded = base64_decode($data_value);
                $value_dencrypted =  Security::decrypt($value_base64_decoded, $encryption_key);
                $employee[$named_index] = $value_dencrypted;
            }
        }

        $this->set(compact('employee','result'));
    }

    public function  removeSpecialChars($string) {
        $not_allowed= array ("á","é","í","ó","ú","Á","É","Í","Ó","Ú","ñ","À","Ã","Ì","Ò","Ù","Ã™","Ã ","Ã¨","Ã¬","Ã²","Ã¹","ç","Ç","Ã¢","ê","Ã®","Ã´","Ã»","Ã‚","ÃŠ","ÃŽ","Ã”","Ã›","ü","Ã¶","Ã–","Ã¯","Ã¤","«","Ò","Ã","Ã„","Ã‹");
        $allowed= array ("a","e","i","o","u","A","E","I","O","U","n","N","A","E","I","O","U","a","e","i","o","u","c","C","a","e","i","o","u","A","E","I","O","U","u","o","O","i","a","e","U","I","A","E");
        $text = str_replace($not_allowed, $allowed ,$string);
        return $text;
        }

    public function add($company_id = null){
        $contractor_employee = $this->ContractorEmployees->newEmptyEntity();
        $black_list = TableRegistry::getTableLocator()->get('VisitBlackListMembers');
        if($this->request->is('post')) {
        $session = $this->getRequest()->getSession();
        $encryption_key = $session->read('UserInfo.Key');
            $data = $this->request->getData();
            foreach($data as $key=>$element ) {
                if (is_string($element)){
                    $element = trim($element);    
                    $data[$key] = $element;
                }
            }
           
            // Validate Black List
            
        $connection_mysql = ConnectionManager::get('default');
        
        $name_clean = $this->removeSpecialChars($data['name']);
        $last_name_clean = $this->removeSpecialChars($data['last_name']);
        $second_last_name_clean = $this->removeSpecialChars($data['second_last_name']);
        $second_last_name = "";
        $first_name =  base64_encode(strtolower($name_clean));
        $last_name =  base64_encode(strtolower($last_name_clean));
        if($second_last_name_clean != "") {
            $second_last_name =  base64_encode(strtolower($second_last_name_clean));
        }
        $select = "SELECT * FROM visit_black_list_members where name = '$first_name' 
        and last_name = '$last_name' and second_last_name = '$second_last_name' and status = 'Activo'";
        $user_query = $connection_mysql->execute($select)->fetchAll('assoc');
        if(count($user_query) > 0 ) {
            $this->Flash->error(__('Error , usuario no permitido para agregar, concate a EHS..'));
            return $this->redirect(['controller'=>'visit-companies','action' => 'view', $company_id]);

        }
        $ssn  = base64_encode($data['ssn']);
        $select = "SELECT * FROM visit_black_list_members where ssn = '$ssn' and status = 'Activo'";
        $ssn_query = $connection_mysql->execute($select)->fetchAll('assoc');
        if(count($ssn_query) > 0 ) {
            $this->Flash->error(__('Error , usuario no permitido para agregar, concate a EHS..'));
            return $this->redirect(['controller'=>'visit-companies','action' => 'view', $company_id]);

        }
            
            // Validate Black List


            $entity_values = array();
            $first_name_enc = Security::encrypt($data['name'], $encryption_key);
            $first_name_enc_base64  = base64_encode($first_name_enc);
            $entity_values['name'] = $first_name_enc_base64;
           
            $last_name_enc = Security::encrypt($data['last_name'], $encryption_key);
            $last_name_enc_base64  = base64_encode($last_name_enc);
            $entity_values['last_name'] = $last_name_enc_base64;
            
            $second_last_name_enc = Security::encrypt($data['second_last_name'], $encryption_key);
            $second_last_name_enc_base64  = base64_encode($second_last_name_enc);
            $entity_values['second_last_name'] = $second_last_name_enc_base64;
            if($data['second_last_name'] == "" ) {
                unset($data['second_last_name']);
                unset($entity_values['second_last_name'] );
            }
            

            $ssn_enc = Security::encrypt($data['ssn'], $encryption_key);
            $ssn_enc_base64  = base64_encode($ssn_enc);
            $entity_values['ssn'] = $ssn_enc_base64;

          /*  $members = $black_list->find()->where(['name =' => $first , 'last_name =' => $last_name, 'second_last_name = ' => $second_last_name , 'ssn' => $ssn] )->toList();
          if(!empty($members)){
            $this->Flash->error(__('No se puede agregar al Usuario'));
            return $this->redirect(['action' => 'add']);
          }*/
          
            $contractor_employee = $this->ContractorEmployees->patchEntity($contractor_employee , $entity_values );
            $contractor_employee->status = 'Registrado';
            $contractor_employee->visit_company_id = $data['visit_company_id'] ;

            $contractor_employee->insert_date = FrozenTime::now();

            if($this->ContractorEmployees->save($contractor_employee)) {
                $this->Flash->success(__('Persona Agregada a la Plantilla'));
                return $this->redirect(['controller'=>'visit-companies','action' => 'view' ,   $contractor_employee->visit_company_id]);
            }
            $this->Flash->error(__('Unable to add your visitor.'));
        }

                $this->set('contractor_employee', $contractor_employee);
                $this->set('company_id', $company_id);

    }

   

    
    public function takeTest($employee_id = '' ) {
        $session = $this->getRequest()->getSession();
        $company_id = $session->read('UserInfo.CompanyID');
        $employee_documents = TableRegistry::getTableLocator()->get('ContractorEmployeeDocuments');
        $query_documents = $employee_documents->find('all')->select('id')->where(['ContractorEmployeeDocuments.contractor_employee_id' => $employee_id , 
        'ContractorEmployeeDocuments.status IN' =>  ['No Valido','Registrado'] ]);
        $number = $query_documents->count();
        if($number > 0 ){
            $this->Flash->error(__('El Usuario tiene ['.$number.'] Documentos Pendientes de Validar y No puede Realizar la Evaluacion '));
            return $this->redirect(['controller'=> 'visit-companies', 'action' => 'view' , $company_id ]);
        }
        
        $company_table = TableRegistry::getTableLocator()->get('VisitCompanies');
        $query_company = $company_table->find('all')->select(["VisitCompanies.visit_external_type_id" ]) 
        ->where(['VisitCompanies.id' => $company_id]);
        $query_company->enableHydration(false); // Results as arrays instead of entities
        $result_company = $query_company->toList();
        $external_type_id = $result_company[0]['visit_external_type_id'];
       
        $external_type_training = TableRegistry::getTableLocator()->get('ExternalTypeTraining');
        $query = $external_type_training->find('all')->select(["ExternalTypeTraining.contractor_training_id" , 
        "contractor_training.description"])
        ->join([
            'table' => 'contractor_training',
            'type' => 'LEFT',
            'conditions' => 'ExternalTypeTraining.contractor_training_id = contractor_training.id']) 
        ->where(['ExternalTypeTraining.visit_external_type_id' => $external_type_id, 'contractor_training.active' => 1 ]);
        $query->enableHydration(false); // Results as arrays instead of entities
        $result = $query->toList();
             
        $employee_training = TableRegistry::getTableLocator()->get('ContractorEmployeeTraining');
        $available_training= array();
        foreach($result as $key=>$value) {
            $available_training[$value['contractor_training_id']] = $value['contractor_training']['description'];
        
        }

        if($this->request->is('post')) { 

            $data = $this->request->getData();
            $training_id = $data['training_id'];
            $employee = $data['contractor_employee_id'];
            $count_training = $employee_training->find('all')->where(['contractor_employee_id' => $employee , 'contractor_training_id' => $training_id ]);
            $number = $count_training->count();
            if($number >= 2) {
                $this->Flash->error(__('El Usuario Excedio su Numero de Intentos ['.$number.' Intentos]'));
                return $this->redirect(['controller'=> 'visit-companies', 'action' => 'index' ]);

            }
            return $this->redirect(['action' => 'trainingTest' ,  $employee , $training_id]);
        } 
            $this->set('available_training', $available_training);
            $this->set('employee_id', $employee_id);

    }
    public function trainingTest($contractor_employee_id = '' , $training_id = '') {
        $employee_training = TableRegistry::getTableLocator()->get('ContractorEmployeeTraining');
        $count_training = $employee_training->find('all')->where(['contractor_employee_id' => $contractor_employee_id , 'contractor_training_id' => $training_id ]);
        $number = $count_training->count();
        if($number >= 3) {
            $this->Flash->error(__('El Usuario Excedio su Numero de Intentos ['.$number.' Intentos]'));
            return $this->redirect(['controller'=> 'visit-companies', 'action' => 'index' ]);

        }
        
        $session = $this->getRequest()->getSession();
          $company_id = $session->read('UserInfo.CompanyID');
          $encryption_key = $session->read('UserInfo.Key');
        
        $connection = ConnectionManager::get('default');       
        $employees_table = TableRegistry::getTableLocator()->get('ContractorEmployees');
        $training_table = TableRegistry::getTableLocator()->get('ContractorTraining');
        $training_data = $training_table->find('all')->where(['id' => $training_id ,'active' => 1 ])->enableHydration(false)->toList();
        $training_name = $training_data[0]['description'];
        $employee_data = $employees_table->find('all')->where(['id' => $contractor_employee_id ])->enableHydration(false)->toList();
        if(count($employee_data) == 0 ) {
            $this->Flash->error(__('Usuario no Encontrado '));
            return $this->redirect(['controller'=> 'visit-companies' , 'action' => 'view' ,  $company_id ]);

        }

        if($employee_data[0]['visit_company_id'] != $company_id ) {
            $this->Flash->error(__('Información No disponible para tu empresa '));
            return $this->redirect(['controller'=> 'visit-companies' , 'action' => 'view' ,  $company_id ]);

        }
        $name_decrypted = base64_decode($employee_data[0]['name']);
        $name_decrypted =  Security::decrypt($name_decrypted, $encryption_key);

        $last_name_decrypted = base64_decode($employee_data[0]['last_name']);
        $last_name_decrypted =  Security::decrypt($last_name_decrypted, $encryption_key);

        $second_last_name_dec = "" ; 
        if($employee_data[0]['second_last_name'] != '' ) {
            $second_last_name_dec = base64_decode($employee_data[0]['second_last_name']);
            $second_last_name_dec =  Security::decrypt($second_last_name_dec, $encryption_key);
        }

        $employee_name = $name_decrypted." ".$last_name_decrypted." ".$second_last_name_dec;
        /*Empty Data Validation .
            
        print_r($employee_data);
        exit;
        */
        if($this->request->is('post')) {
            $correct_answers = 0;
            $data = $this->request->getData();
       
            return $this->redirect(['action' => 'getScore' ,  serialize($data) ]);

        }
        $answers = array();
        $questions = $connection->newQuery()
                    ->select('contractor_questions.id, contractor_questions.text')
                    ->from('contractor_questions')
                    ->join([
                        'table' => 'contractor_training_questions',
                        'alias' => 'c',
                        'type' => 'LEFT',
                        'conditions' => 'contractor_questions.id = c.question_id'
                    ]) 
                    ->where(['contractor_questions.active' => 1 , 'c.training_id' => $training_id])
                    ->order('RAND()')
                    ->limit('10') 
                    ->execute()
                    ->fetchAll('assoc');
        foreach($questions as $result ) {
            $answers_query = $connection->newQuery()
            ->select('id, text ')
            ->from('contractor_question_answers')
            ->where(['active' => 1, 'contractor_question_id' =>  $result['id'] ])
            ->order('RAND()') 
            ->execute()
            ->fetchAll('assoc');
            $answers[$result['id']] = $answers_query;
        }
        $this->set('questions', $questions);
        $this->set('answers', $answers);
        $this->set('training_name', $training_name);
        $this->set('employee_name', $employee_name);
        $this->set('training_id', $training_id);
        $this->set('employee_id', $contractor_employee_id);

    }



    public function getScore($data ) {
        $connection = ConnectionManager::get('default');       
        $training_log = TableRegistry::getTableLocator()->get('ContractorEmployeeTraining');

        $data = unserialize($data);
            $training_id = $data['training_id'];
            unset( $data['training_id']);
            $employee_id = $data['employee_id'];
            unset( $data['employee_id']);
            $correct_answers = 0;
            $total_questions = count($data);
            $question_count = 1 ;
            $results = array(); 
            foreach($data as $question => $answer ) {
              //  $check_answer_query = "select correct_answer from contractor_question_answers where contractor_question_id = $question and id = $answer " ;
                $result = $connection->newQuery()
                ->select('contractor_question_answers.correct_answer , contractor_question_answers.text as answer_text, c.text as question_text')
                ->from('contractor_question_answers')
                ->join([
                    'table' => 'contractor_questions',
                    'alias' => 'c',
                    'type' => 'LEFT',
                    'conditions' => 'c.id = contractor_question_answers.contractor_question_id',
                ])
                ->where(['contractor_question_id' => $question , 'contractor_question_answers.id' => $answer])
                ->execute()
                ->fetchAll('assoc');

                $results[$question_count]['question'] = $result[0]['question_text'];
                $results[$question_count]['answer'] = $result[0]['answer_text'];
                $results[$question_count]['correct'] = $result[0]['correct_answer'];

                if($result[0]['correct_answer'] == 1 ) {
                    $correct_answers = $correct_answers + 1 ;
                }
              
                $question_count+=1;
                
            }
            $score = ($correct_answers/$total_questions ) * 100 ; 
            $query = $training_log->query();
            $query->insert(['contractor_employee_id', 'contractor_training_id' , 'date_scored' , 'score'])
                ->values([
                    'contractor_employee_id' => $employee_id,
                    'contractor_training_id' => $training_id,
                    'date_scored' => FrozenTime::now() ,
                    'score' => $score

                ])
                ->execute();
            $this->set('total_questions', $total_questions);
            $this->set('incorrect_count', $total_questions - $correct_answers);
            $this->set('correct', $correct_answers);
            $this->set('score', $score);
            $this->set('results', $results);
      
    }

    
    public function generateQr($contractor_employee_id = "" ) {
        $result = $this->ContractorEmployees->find('all')->where(['id' => $contractor_employee_id ])->enableHydration(false)->toList();
        $session = $this->getRequest()->getSession();
        $company = $session->read('UserInfo.CompanyID');
        if(count($result) == 0 ) {
            $this->Flash->error(__('Usuario No Encontrado'));
            return $this->redirect(['controller'=> 'visit-companies' , 'action' => 'view' , $company ]);
        }
        $employee_company  = $result[0]['visit_company_id'] ;
        if($company != $employee_company) {
            $this->Flash->error(__('No Permitido'));
            return $this->redirect(['controller'=> 'visit-companies' , 'action' => 'view' , $company ]);
        }

        $status = $result[0]['status'] ;
        if($status != 'Activo') {
            $this->Flash->error(__('Usuario No Validado'));
            return $this->redirect(['controller'=> 'visit-companies' , 'action' => 'view' , $company ]);
        }
        $employee_information = $this->decryptEmployeeData($result,$this->encryption_key );
       

        $url = "http://zmxvm-intranet/VisitorsApp/contractor-attendance/add/".$contractor_employee_id;
        $url = urlencode($url);
        $qr = "https://chart.googleapis.com/chart?chs=300x300&cht=qr&chl=".$url."&choe=UTF-8";
        $this->set('url' , $qr);
        $this->set('company' , $company);
        $this->set('employee' , $employee_information[0]);

    }


    public function decryptEmployeeData($employee_inforrmation  = array() , $encryption_key  = ""  )  {
        $employee_inforrmation[0]['name'] = Security::decrypt(base64_decode($employee_inforrmation[0]['name']), $encryption_key);
        $employee_inforrmation[0]['last_name'] = Security::decrypt(base64_decode($employee_inforrmation[0]['last_name']), $encryption_key);
        if($employee_inforrmation[0]['second_last_name'] != NULL ) {
            $employee_inforrmation[0]['second_last_name'] = Security::decrypt(base64_decode($employee_inforrmation[0]['second_last_name']), $encryption_key);
        }

        return $employee_inforrmation;

    }
}

