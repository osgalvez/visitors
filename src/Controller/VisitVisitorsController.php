<?php
// src/Controller/ArticlesController.php
namespace App\Controller;
use Cake\ORM\TableRegistry;

class VisitVisitorsController extends AppController
{
    public function initialize(): void
    {
        parent::initialize();

        $this->loadComponent('Paginator');
        $this->loadComponent('Flash'); // Include the FlashComponent
    }

    public function index()
    {
        $this->loadComponent('Paginator');
        $visitors = $this->VisitVisitors->find('all')
            ->select(['VisitVisitors.id', 'VisitVisitors.first_name' , 'VisitVisitors.last_name', 'VisitVisitors.second_last_name',
            'VisitVisitors.phone' , 'VisitVisitors.email' , 'VisitVisitors.company' , 'VisitVisitors.other_country' , 'visit_allowed_countries.country_name']) 
            ->join([
            'table' => 'visit_allowed_countries',
            'type' => 'LEFT',
            'conditions' => 'VisitVisitors.nationality = visit_allowed_countries.id',]) ;
       
        $this->set(compact('visitors'));
    }

    public function view($id = null ) {
        $visitors = TableRegistry::getTableLocator()->get('VisitIdDocuments');
        $query = $visitors->find()
        ->where(['VisitIdDocuments.visit_visitor_id' => $id]);
        $query->enableHydration(false); // Results as arrays instead of entities
        $result = $query->toList(); 
        $visitor = $this->VisitVisitors->findById($id)->firstOrFail();
        $this->set(compact('visitor','result'));
    }

    public function add(){

        $countries = TableRegistry::getTableLocator()->get('VisitAllowedCountries');
        $black_list = TableRegistry::getTableLocator()->get('VisitBlackListMembers');

        $query = $countries->find()->where(['VisitAllowedCountries.Status' => 'Activo']);
        
        $query->enableHydration(false); // Results as arrays instead of entities
        $result = $query->toList(); 
        foreach($result as $country => $data) {
            $country_result[$data['id']] = $data['country_name'];   
        }
        // Execute the query and return the array
        $this->set(compact('country_result'));


        $visitor = $this->VisitVisitors->newEmptyEntity();
      
        if($this->request->is('post')) {

            $data = $this->request->getData();
            foreach($data as $key=>$element ) {
                if (is_string($element)){
                    $element = trim($element);    
                    $data[$key] = $element;
                }
            }
            $first = $data['first_name'];
            $last_name = $data['last_name'];
            $second_last_name = $data['second_last_name'];
            
            $members = $black_list->find()->where(['name =' => $first , 'last_name =' => $last_name, 'second_last_name = ' => $second_last_name])->toList();
          if(!empty($members)){
            $this->Flash->error(__('No se puede agregar al Usuario'));
            return $this->redirect(['action' => 'add']);
          }
          
            $visitor = $this->VisitVisitors->patchEntity($visitor , $data);
            $nationality = $data['nationality'];
            
            if($nationality == 5 ) {
                $visitor->other_country = $data['other_country'];
                $value = $data['support_email'];
                if(is_object($value ) )  {
                    if($value->getSize() > 0 ) {
                        $target_path = (WWW_ROOT.'img_upload');
                        $file_path = $target_path.DS.time().$value->getclientFilename();
                        $value->moveTo($file_path); 
                        $img_base64 = base64_encode(file_get_contents($file_path));
                        $visitor->support_email = $img_base64;
                        $visitor->support_file_type = $value->getclientMediaType();
                        unlink($file_path);

                    }
                    else {
                        $this->Flash->error(__('El campo de archivo no puede ir vacio'));
                        return $this->redirect(['action' => 'add']);
                    }
                    
                }
            }

            if($this->VisitVisitors->save($visitor)) {
                $this->Flash->success(__('Visitante registrado exitosamente'));
                $visitorid = $visitor->id;
             
                return $this->redirect(['controller'=>'visit_visitor_visits','action' => 'add',  $visitor->id  ]);
            }
            $this->Flash->error(__('No se pudo registrar.'));
        }

                $this->set('visitor', $visitor);
    }
}