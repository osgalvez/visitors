<?php
// src/Controller/ArticlesController.php
namespace App\Controller;
use Cake\ORM\TableRegistry;

class VisitScheduledVisitsController extends AppController
{
    public function initialize(): void
    {
        parent::initialize();
        $this->loadComponent('Paginator');
        $this->loadComponent('Flash'); // Include the FlashComponent
    }
    public function index()
    {
        $this->loadComponent('Paginator');
        $this->VisitScheduledVisits->find('all', ['contain' => ['VisitVisitor']]);
        $scheduled_visits = $this->Paginator->paginate($this->VisitScheduledVisits->find());
        $this->set(compact('scheduled_visits'));
    }

    public function view($id = null ) {
        $visitors = TableRegistry::getTableLocator()->get('VisitIdDocuments');
        $query = $visitors->find()
        ->where(['VisitIdDocuments.visit_visitor_id' => $id]);
        $query->enableHydration(false); // Results as arrays instead of entities
        $result = $query->toList(); 
        $visitor = $this->VisitVisitors->findById($id)->firstOrFail();
        $this->set(compact('visitor','result'));
    }

    public function add(){
        $visitors = TableRegistry::getTableLocator()->get('VisitVisitors');
        $query = $visitors->find();
        
        $query->enableHydration(false); // Results as arrays instead of entities
        $result = $query->toList(); 
        foreach($result as $visitor => $data) {
            $visitor_result[$data['id']] = $data['first_name']. " " . $data['last_name']." " . $data['second_last_name']. " - " . $data['company'];   
        }
        // Execute the query and return the array
        $this->set(compact('visitor_result'));
        $scheduled_visit = $this->VisitScheduledVisits->newEmptyEntity();
      
        if($this->request->is('post')) {
    
            
            $scheduled = $this->VisitScheduledVisits->patchEntity($scheduled_visit , $this->request->getData());
            $session = $this->getRequest()->getSession();
            $scheduled->insert_user_id =  $session->read('UserInfo.UserID');           
            $scheduled->area_code = $session->read('UserInfo.AreaCode');
            $scheduled->area = $session->read('UserInfo.Area');
            $scheduled->visited_person_name =  $session->read('UserInfo.CommonName');
            $scheduled->visited_person_id =  $session->read('UserInfo.UserID');
            $scheduled->status =  'Registrado';

            if($this->VisitScheduledVisits->save($scheduled)) {
                $this->Flash->success(__('Visita Programada'));
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('Ocurrio un error al programar tu visita'));
        }

                $this->set('scheduled_visit', $scheduled_visit);
    }
}