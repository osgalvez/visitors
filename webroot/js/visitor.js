$(document).ready(function() {
    $('table').DataTable({
        "language": {
            
	"sProcessing":     "Procesando...",
                "sLengthMenu":     "Mostrar _MENU_ registros",
                "sZeroRecords":    "No se encontraron resultados",
                "sEmptyTable":     "Ningún dato disponible en esta tabla =(",
                "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
                "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                "sInfoPostFix":    "",
                "sSearch":         "Buscar:",
                "sUrl":            "",
                "sInfoThousands":  ",",
                "sLoadingRecords": "Cargando...",
                "oPaginate": {
                    "sFirst":    "Primero",
                    "sLast":     "Último",
                    "sNext":     "Siguiente",
                    "sPrevious": "Anterior"
                },
                "oAria": {
                    "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                },
                "buttons": {
                    "copy": "Copiar",
                    "colvis": "Visibilidad"
                }

            } ,
            "order": [[ 1, "desc" ]]
    });

    $('#select-country').change(function(){
        let country_value = $(this).val();
        if(country_value == 1){
            $('#support_email').removeAttr('disabled');
            $('#support_email').attr('required','required');
            $('#other_country').removeAttr('disabled');
            $('#other_country').attr('required','required');

        }
        else {
            $('#support_email').attr('disabled','disabled');
            $('#support_email').removeAttr('required');
            $('#other_country').attr('disabled','disabled');
            $('#other_country').removeAttr('required');
            
        }
    });
} );